
package DAO;

import Cont.clases.BG.ER.Comprobantes;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;


public class Implements_Comprobantes {
    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 500;
    BufferedWriter bw ;
    BufferedWriter bwl ;

    private void open() throws FileNotFoundException, IOException{
        fHead = new File("Comprobantes.head");
        fData = new File("Comprobantes.data");
        
        
        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");
        
        if(rafHead.length() <= 0){
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }
        
    }

    private void close() throws IOException {
        if (rafHead != null) {
            rafHead.close();
        }
        if (rafData != null) {
            rafData.close();
        }  
    }

  
    public void create(Comprobantes e) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();

        long pos = k * SIZE;

        rafData.seek(pos);

        rafData.writeInt(++k);
        rafData.writeUTF(e.getNombre());
        rafData.writeUTF(e.getEstado());
        rafData.writeUTF(e.getDescrpcion());
        rafData.writeUTF(e.getCuenta_afecta());
        rafData.writeUTF(e.getCuenta_auxiliar());
        rafData.writeDouble(e.getCantidad_deb());
        rafData.writeDouble(e.getCantidad_hab());
        rafData.writeUTF(e.getCuentaER_afect());
        rafData.writeDouble(e.getCantidad_ER());
        rafData.writeUTF(pars(e.getFecha()));
        
        

        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        rafHead.seek(hpos);
        rafHead.writeInt(k);
        close();
    }
    public List<Comprobantes> showAll() throws IOException {
        open();
        List<Comprobantes> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);
            
            Comprobantes e = new Comprobantes();
            e.setId(rafData.readInt());
            e.setNombre(rafData.readUTF());
            e.setEstado(rafData.readUTF());
            e.setDescrpcion(rafData.readUTF());
            e.setCuenta_afecta(rafData.readUTF());
            e.setCuenta_auxiliar(rafData.readUTF());
            e.setCantidad_deb(rafData.readDouble());
             e.setCantidad_hab(rafData.readDouble());
             e.setCuentaER_afect(rafData.readUTF());
             e.setCantidad_ER(rafData.readDouble());
            e.setFecha(ParseFecha(rafData.readUTF()));
            
            estudiantes.add(e);
        }
        close();
        return estudiantes;
    }
    public boolean delete(Comprobantes t) throws IOException {
        open();
        List<Comprobantes> e = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        for (Comprobantes em : e){
            if(em.getId()!= t.getId()){
            create(em);    
            }
        }
        
        close();
        return true;
        

    }
     public int update(Comprobantes e) throws IOException {
        open();
        List<Comprobantes> g = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        g.set(e.getId()-1,e);
        for (Comprobantes em : g){
            create(em);
        }
        
        close();
        return 0;
        
    }
     public static Date ParseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        return fechaDate;
    }
     public static String pars(Date myDate){
         String fecha= new SimpleDateFormat("dd-MM-yyyy").format(myDate);
         return fecha;
     }
     
     
    
}
