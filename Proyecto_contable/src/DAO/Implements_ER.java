
package DAO;

import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Implements_ER {
     private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 220;
    BufferedWriter bw ;
    BufferedWriter bwl ;

    private void open() throws FileNotFoundException, IOException{
        fHead = new File("Estatdo.head");
        fData = new File("Estado.data");
        
        
        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");
        
        if(rafHead.length() <= 0){
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }
        
    }

    private void close() throws IOException {
        if (rafHead != null) {
            rafHead.close();
        }
        if (rafData != null) {
            rafData.close();
        }  
    }

  
    public void create(Estado_Result e) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();

        long pos = k * SIZE;

        rafData.seek(pos);

        rafData.writeInt(++k);
        rafData.writeDouble(e.getVenta());
        rafData.writeDouble(e.getCosto_venta());
        rafData.writeDouble(e.getGasto_gen());
        rafData.writeDouble(e.getDividendos_comunes());
        rafData.writeDouble(e.getDividendos_preferentes());
        rafData.writeUTF(pars(e.getFecha_inicio()));
        rafData.writeUTF(pars(e.getFecha_fin()));
        
        

        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        rafHead.seek(hpos);
        rafHead.writeInt(k);
        close();
    }
    public List<Estado_Result> showAll() throws IOException {
        open();
        List<Estado_Result> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);
            
            Estado_Result e = new Estado_Result();
            e.setId(rafData.readInt());
            e.setVenta(rafData.readDouble());
            e.setCosto_venta(rafData.readDouble());
            e.setGasto_gen(rafData.readDouble());
           
            e.setDividendos_comunes(rafData.readDouble());
             e.setDividendos_preferentes(rafData.readDouble());
            e.setFecha_inicio(ParseFecha(rafData.readUTF()));
            e.setFecha_fin(ParseFecha(rafData.readUTF()));
            
            
            estudiantes.add(e);
        }
        close();
        return estudiantes;
    }
    public boolean delete(Estado_Result t) throws IOException {
        open();
        List<Estado_Result> e = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        for (Estado_Result em : e){
            if(em.getId()!= t.getId()){
            create(em);    
            }
        }
        
        close();
        return true;
        

    }
     public int update(Estado_Result e) throws IOException {
        open();
        List<Estado_Result> g = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        g.set(e.getId()-1,e);
        for (Estado_Result em : g){
            create(em);
        }
        
        close();
        return 0;
        
    }
     public static Date ParseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        return fechaDate;
    }
     public static String pars(Date myDate){
         String fecha= new SimpleDateFormat("dd-MM-yyyy").format(myDate);
         return fecha;
     }
     
}
