package DAO;

import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Gastos;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Implements_gastos {

    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 2476;
    BufferedWriter bw;
    BufferedWriter bwl;

    private void open() throws FileNotFoundException, IOException {
        fHead = new File("Gastos.head");
        fData = new File("Gastos.data");

        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");

        if (rafHead.length() <= 0) {
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }

    }

    private void close() throws IOException {
        if (rafHead != null) {
            rafHead.close();
        }
        if (rafData != null) {
            rafData.close();
        }
    }

    public void create(Gastos e) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();

        long pos = k * SIZE;

        rafData.seek(pos);

        rafData.writeInt(++k);
        rafData.writeUTF(e.getNombre());
        rafData.writeUTF(pars(e.getFecha_Incio()));
        rafData.writeUTF(pars(e.getUltimaFechaCobrada()));
        rafData.writeUTF(pars(e.getFecha_Final()));
        rafData.writeInt(e.getPeriodo());
        //rafData.write(e.getDias());
        if (e.getNombre().equals("Gastos por intereses")) {
            rafData.write(getByteArray(e.getInteresMensual()));
            rafData.write(getByteArray(e.getPagoPrincipal()));
        }
        rafData.writeDouble(e.getMensualidad());
        rafData.writeBoolean(e.isActive());

        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        rafHead.seek(hpos);
        rafHead.writeInt(k);
        close();
    }

    public List<Gastos> showAll() throws IOException {
        open();
        List<Gastos> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);

            Gastos e = new Gastos();
            e.setId(rafData.readInt());
            e.setNombre(rafData.readUTF());
            e.setFecha_Incio(ParseFecha(rafData.readUTF()));
            e.setUltimaFechaCobrada(ParseFecha(rafData.readUTF()));
            e.setFecha_Final(ParseFecha(rafData.readUTF()));
            int periodo = rafData.readInt();
            e.setPeriodo(periodo);
            //e.setDias(rafData.read());
            if (e.getNombre().equals("Gastos por intereses")) {
                byte[] arrayInteres = new byte[(periodo + 1) * 8];
                rafData.read(arrayInteres);
                double[] valores = getDoubleArray(arrayInteres);

                e.setInteresMensual(valores);

                byte[] arrayPago = new byte[(periodo + 1) * 8];
                rafData.read(arrayPago);
                double[] valorePago = getDoubleArray(arrayPago);

                e.setPagoPrincipal(valorePago);
            }
            e.setMensualidad(rafData.readDouble());
            e.setActive(rafData.readBoolean());

            estudiantes.add(e);
        }
        close();
        return estudiantes;
    }

    public boolean delete(Comprobantes t) throws IOException {
        open();
        List<Gastos> e = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        for (Gastos em : e) {
            if (em.getId() != t.getId()) {
                create(em);
            }
        }

        close();
        return true;

    }

    public int update(Gastos e) throws IOException {
        open();
        List<Gastos> g = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        g.set(e.getId() - 1, e);
        for (Gastos em : g) {
            create(em);
        }

        close();
        return 0;

    }

    public static Date ParseFecha(String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return fechaDate;
    }

    public static String pars(Date myDate) {
        String fecha = new SimpleDateFormat("dd-MM-yyyy").format(myDate);
        return fecha;
    }

    private static byte[] getByteArray(double[] array) {
        ByteBuffer bb = ByteBuffer.allocate(array.length * 8);
        double d = 0;
        for (int i = 0; i < array.length; i++) {
            d = array[i];
            bb.putDouble(d);
        }
        return bb.array();
    }

    private static double[] getDoubleArray(byte[] array) {
        ByteBuffer bb = ByteBuffer.wrap(array);
        double[] doubles = new double[array.length / 8];
        for (int i = 0; i < doubles.length; i++) {
            doubles[i] = bb.getDouble();
        }
        return doubles;
    }
}
