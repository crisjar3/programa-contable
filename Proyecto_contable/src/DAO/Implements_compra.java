

package DAO;

import Cont.clases.BG.ER.COmpra;
import Cont.clases.BG.ER.Cuentas_balance;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;


public class Implements_compra {

    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 220;
    BufferedWriter bw ;
    BufferedWriter bwl ;

    private void open() throws FileNotFoundException, IOException{
        fHead = new File("compra.head");
        fData = new File("compra.data");
        
        
        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");
        
        if(rafHead.length() <= 0){
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }
        
    }

    private void close() throws IOException {
        if (rafHead != null) {
            rafHead.close();
        }
        if (rafData != null) {
            rafData.close();
        }  
    }

  
    public void create(COmpra e) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();

        long pos = k * SIZE;

        rafData.seek(pos);

        rafData.writeInt(++k);
        rafData.writeDouble(e.getDinero());
        
        

        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        rafHead.seek(hpos);
        rafHead.writeInt(k);
        close();
    }
    public COmpra showAll() throws IOException {
        open();
        List<COmpra> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);
            
            COmpra e = new COmpra();
            e.setId(rafData.readInt());
            e.setDinero(rafData.readDouble());
            
            estudiantes.add(e);
        }
        close();
        return estudiantes.get(0);
    }
     public List<COmpra> showAlli() throws IOException {
        open();
        List<COmpra> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);
            
            COmpra e = new COmpra();
            e.setId(rafData.readInt());
            e.setDinero(rafData.readDouble());
            
            estudiantes.add(e);
        }
        close();
        return estudiantes;
    }
    
     public int update(COmpra e) throws IOException {
        open();
        List<COmpra> g = showAlli();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        g.set(e.getId()-1,e);
        for (COmpra em : g){
            create(em);
        }
        
        close();
        return 0;
        
    }
}
