/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Cont.clases.BG.ER.Cuentas_balance;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;


public class Implements_balance {
    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 220;
    BufferedWriter bw ;
    BufferedWriter bwl ;

    private void open() throws FileNotFoundException, IOException{
        fHead = new File("Balance.head");
        fData = new File("Balance.data");
        
        
        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");
        
        if(rafHead.length() <= 0){
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }
        
    }

    private void close() throws IOException {
        if (rafHead != null) {
            rafHead.close();
        }
        if (rafData != null) {
            rafData.close();
        }  
    }

  
    public void create(Cuentas_balance e) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt();
        int k = rafHead.readInt();

        long pos = k * SIZE;

        rafData.seek(pos);

        rafData.writeInt(++k);
        rafData.writeUTF(e.getNombre_cuenta());
        rafData.writeUTF(e.getTipo());
        rafData.writeUTF(e.getTiempo());
        rafData.writeUTF(e.getFinali());
        rafData.writeDouble(e.getDinero());
        rafData.writeDouble(e.getDinero_inicial());
        

        rafHead.seek(0);
        rafHead.writeInt(++n);
        rafHead.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        rafHead.seek(hpos);
        rafHead.writeInt(k);
        close();
    }
    public List<Cuentas_balance> showAll() throws IOException {
        open();
        List<Cuentas_balance> estudiantes = new ArrayList<>();
        rafHead.seek(0);
        int n = rafHead.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            rafHead.seek(hpos);

            int index = rafHead.readInt();
            long dpos = (index - 1) * SIZE;
            rafData.seek(dpos);
            
            Cuentas_balance e = new Cuentas_balance();
            e.setId_cuenta(rafData.readInt());
            e.setNombre_cuenta(rafData.readUTF());
            e.setTipo(rafData.readUTF());
            e.setTiempo(rafData.readUTF());
            e.setFinali(rafData.readUTF());
            e.setDinero(rafData.readDouble());
             e.setDinero_inicial(rafData.readDouble());
            estudiantes.add(e);
        }
        close();
        return estudiantes;
    }
    public boolean delete(Cuentas_balance t) throws IOException {
        open();
        List<Cuentas_balance> e = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        for (Cuentas_balance em : e){
            if(em.getId_cuenta()!= t.getId_cuenta()){
            create(em);    
            }
        }
        
        close();
        return true;
        

    }
     public int update(Cuentas_balance e) throws IOException {
        open();
        List<Cuentas_balance> g = showAll();
        BufferedWriter bw = new BufferedWriter(new FileWriter(fData));
        BufferedWriter bwl = new BufferedWriter(new FileWriter(fHead));
        bw.write("");
        bwl.write("");
        bw.close();
        bwl.close();
        g.set(e.getId_cuenta()-1,e);
        for (Cuentas_balance em : g){
            create(em);
        }
        
        close();
        return 0;
        
    }
}
