/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Cont.clases.BG.ER.Cuentas_balance;
import DAO.Implements_balance;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Cristhian Joel Gomez
 */
public class Tabmod extends AbstractTableModel {

    private List<String> columnNames;
    private List<Cuentas_balance> data;
    private Implements_balance empleado;

    public Tabmod() {
        super();
        columnNames = new ArrayList<>();
        data = new ArrayList<>();
        empleado = new Implements_balance();
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (data.isEmpty()) {
            return null;
        }

        if (rowIndex < 0 || rowIndex >= data.size()) {
            return null;
        }

        List<String> empleado = data.get(rowIndex).toList();

        if (columnIndex < 0 || columnIndex >= empleado.size()) {
            return null;
        }

        return empleado.get(columnIndex);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (row < 0 || row >= data.size()) {
            return;
        }
        List<String> rowEmpleado = data.get(row).toList();
        if (col < 0 || col >= rowEmpleado.size()) {
            return;
        }

        rowEmpleado.set(col, value.toString());
        fireTableCellUpdated(row, col);
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    public int addRow() {
        return addRow(new Cuentas_balance());
    }

    public int addRow(Cuentas_balance row) {
        data.add(row);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
        return data.size() - 1;
    }

    public void deleteRow(int row) throws IOException {
        if (row < 0 || row >= data.size()) {
            return;
        }

        
            empleado.delete(data.get(row));
       
        data.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void loadFromFile() throws IOException {

        data = empleado.showAll();

        String[] names = {"ID", "Nombre de cuenta", "Tipo de cuenta", "Clasificacion de tiempo",
            "clase", "Dinero","Inicial"};
        columnNames = Arrays.asList(names);
    }
    
    public void loadFromFileo(List<Cuentas_balance> e) throws IOException {

        data = e;

         String[] names = {"ID", "Nombre de cuenta", "Tipo de cuenta", "Clasificacion de tiempo",
            "clase", "Dinero"};
        columnNames = Arrays.asList(names);
    }

   
}


