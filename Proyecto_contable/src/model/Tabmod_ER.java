/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_ER;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class Tabmod_ER extends AbstractTableModel {

    private List<String> columnNames;
    private List<Estado_Result> data;
    private Implements_ER empleado;

    public Tabmod_ER() {
        super();
        columnNames = new ArrayList<>();
        data = new ArrayList<>();
        empleado = new Implements_ER();
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (data.isEmpty()) {
            return null;
        }

        if (rowIndex < 0 || rowIndex >= data.size()) {
            return null;
        }

        List<String> empleado = data.get(rowIndex).toList();

        if (columnIndex < 0 || columnIndex >= empleado.size()) {
            return null;
        }

        return empleado.get(columnIndex);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (row < 0 || row >= data.size()) {
            return;
        }
        List<String> rowEmpleado = data.get(row).toList();
        if (col < 0 || col >= rowEmpleado.size()) {
            return;
        }

        rowEmpleado.set(col, value.toString());
        fireTableCellUpdated(row, col);
    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    public int addRow() {
        return addRow(new Estado_Result());
    }

    public int addRow(Estado_Result row) {
        data.add(row);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
        return data.size() - 1;
    }

    public void deleteRow(int row) throws IOException {
        if (row < 0 || row >= data.size()) {
            return;
        }

        empleado.delete(data.get(row));

        data.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void loadFromFile() throws IOException {

        data = empleado.showAll();
        String[] names = {"ID", "Venta", "Costo_venta", "Gastos generales",
            "Imp_venta", "dividendos_preferentes", "Dividendos comunes", "Fecha_inicio", "Fecha_fin"};
        columnNames = Arrays.asList(names);
    }

    public void loadFromFileo(List<Estado_Result> e) throws IOException {

        data = e;

        String[] names = {"ID", "Venta", "Costo_venta", "Gastos generales",
            "Imp_venta", "dividendos_preferentes", "Dividendos comunes", "Fecha_inicio", "Fecha_fin"};
        columnNames = Arrays.asList(names);
    }
}
