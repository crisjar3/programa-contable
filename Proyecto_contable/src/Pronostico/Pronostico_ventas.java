

package Pronostico;

import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_ER;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Pronostico_ventas {
    
    public static double prono(int mes){
        try {
            Implements_ER imp = new Implements_ER();
            List<Estado_Result> list= imp.showAll();
            double IdSum = 0,VentSum = 0,IdxId = 0,VentxVent = 0,VentaxId = 0,n=0;
            for(Estado_Result e: list){
                IdSum=IdSum+e.getId();
                n= e.getId();
                VentSum= VentSum+ e.getVenta();
                IdxId=IdxId+e.getId()*e.getId();
                VentxVent= VentxVent+ e.getCosto_venta()*e.getVenta();
                VentaxId= VentaxId+ e.getVenta()*e.getId();
            }
            
            double FormA=0,FormB=0,FormC=0;
            
            FormB=(n*VentaxId-IdSum*VentSum)/
                    (n*IdxId-IdSum*IdSum);
            FormA=(VentSum-FormB*IdSum)/n;
            
            FormC= (FormB*n)/VentSum;
            
            double pron =FormA/*+FormB*mes;*/;
            return pron;
        } catch (IOException ex) {
            Logger.getLogger(Pronostico_ventas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    

}
