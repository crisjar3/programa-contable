

package Actualiza;

import Cont.clases.BG.ER.COmpra;
import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import Cont.clases.BG.ER.Gastos;
import DAO.Implements_Comprobantes;
import DAO.Implements_ER;
import DAO.Implements_balance;
import DAO.Implements_compra;
import DAO.Implements_gastos;
import Vista.BG.Admin_cuentas_balance;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class actualizaciones_archivos {
    public static void actualizar() {
        try {
            Implements_gastos impGastos = new Implements_gastos();
            Implements_balance imp = new Implements_balance();
    Implements_Comprobantes impComp = new Implements_Comprobantes();

            List<Gastos> gastos = impGastos.showAll();
            List<Gastos> financiamiento = gastos.stream().filter(g -> (g.getNombre().equals("Gastos por intereses"))).collect(Collectors.toList());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            for (Gastos f : financiamiento) {
                if (f.getFecha_Final().after(formatter.parse(formatter.format(new Date()))) || f.getFecha_Final().equals(formatter.parse(formatter.format(new Date()))) || f.getUltimaFechaCobrada().equals(f.getFecha_Incio())) {
                    f.setActive(true);
                } else {
                    f.setActive(false);
                }
            }
            List<Gastos> financiamientosActivos = financiamiento.stream().filter(d -> (d.isActive() == true)).collect(Collectors.toList());
            for (Gastos fA : financiamientosActivos) {
                //JOptionPane.showMessageDialog(null,"Desde admin-cuentas-balance " + fA.toString());
            
                long meses = ChronoUnit.MONTHS.between(LocalDate.parse(formatter.format(fA.getUltimaFechaCobrada())).atStartOfDay(),
                        LocalDate.parse(formatter.format(new Date())).atStartOfDay());

                long mesesYaCobrados = ChronoUnit.MONTHS.between(LocalDate.parse(formatter.format(fA.getFecha_Incio())).atStartOfDay(),
                        LocalDate.parse(formatter.format(fA.getUltimaFechaCobrada())).atStartOfDay());

                List<Cuentas_balance> cuentaBanco = imp.showAll().stream().filter(c -> (c.getNombre_cuenta().equals("BANCO"))).collect(Collectors.toList());
                Cuentas_balance banco = cuentaBanco.get(0);
                banco.setDinero(Math.round(banco.getDinero() - meses * fA.getMensualidad()));

                Calendar cal = Calendar.getInstance();
                cal.setTime(fA.getUltimaFechaCobrada());
                cal.add(Calendar.MONTH, Math.toIntExact(meses));
                fA.setUltimaFechaCobrada(cal.getTime());

                double pagoPrincipalTotal = 0;
                double interesesTotales = 0;
                for (int i = Math.toIntExact(mesesYaCobrados) + 1; i < Math.toIntExact(meses) + 1; i++) {
                    JOptionPane.showMessageDialog(null, "Entra a la suma");
                    pagoPrincipalTotal = pagoPrincipalTotal + fA.getPagoPrincipal()[i];
                    interesesTotales = interesesTotales + fA.getInteresMensual()[i];
                }

                List<Cuentas_balance> cuentaCuentaXPagar = imp.showAll().stream().filter(c -> (c.getNombre_cuenta().equals("DOCUMENTOS POR PAGAR A LARGO PLAZO"))).collect(Collectors.toList());
                Cuentas_balance cuentaBGCuentXPag = cuentaCuentaXPagar.get(0);
                cuentaBGCuentXPag.setDinero(Math.round(cuentaBGCuentXPag.getDinero() - pagoPrincipalTotal));

                if (meses != 0) {
                    Comprobantes prestamo = new Comprobantes();

                    prestamo.setNombre("Pago cuota de prestamo");
                    prestamo.setCuenta_afecta("BANCO");
                    prestamo.setCuenta_auxiliar("DOCUMENTOS POR PAGAR A LARGO PLAZO");
                    prestamo.setEstado("Haber");
                    prestamo.setDescrpcion("Se realiza pago de principal y pago de intereses");
                    prestamo.setCantidad_deb(Math.round(pagoPrincipalTotal));
                    prestamo.setCantidad_hab(Math.round(meses * fA.getMensualidad()));
                    prestamo.setCuentaER_afect("Gastos por intereses");
                    prestamo.setCantidad_ER(Math.round(interesesTotales));
                    prestamo.setFecha(new Date());

                    impComp.create(prestamo);
                    imp.update(banco);
                    imp.update(cuentaBGCuentXPag);
                    impGastos.update(fA);
                }
            }
        } catch (IOException | ParseException ex) {
            Logger.getLogger(Admin_cuentas_balance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Estado_Result lastEr(){
        Estado_Result ep = null;
        try {
            Implements_ER e= new Implements_ER();
            List<Estado_Result> opi= e.showAll();
             ep= opi.get(opi.size()-1);
            return ep;
        } catch (IOException ex) {
            Logger.getLogger(actualizaciones_archivos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ep;
        
    }
    public static void actER() throws IOException{
        Date fech= new Date();
        String opl=lastEr().getFecha_fin().toString();
            
        if(fech.compareTo(lastEr().getFecha_fin())>0){
            AddEr();
            
        }
    }
    
    public static void AddEr() throws IOException{
        Implements_gastos impGastos = new Implements_gastos();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            Implements_ER e = new Implements_ER();
            Estado_Result ea = new Estado_Result();
            Date Fecha = new Date();
            ea.setVenta(0);
            ea.setCosto_venta(0);
            double gastoFijo = 0d;
            for (Gastos gasto : impGastos.showAll()) {
                LocalDateTime local = LocalDate.parse(formatter.format(new Date())).atStartOfDay();
                if (local.isBefore(LocalDate.parse(formatter.format(gasto.getFecha_Final())).atStartOfDay())) {
                    gastoFijo = gastoFijo + gasto.getMensualidad();
                }
            }
            ea.setGasto_gen(gastoFijo);
            ea.setDividendos_comunes(0);
            ea.setDividendos_comunes(0);
            ea.setImp_venta(0);
            ea.setFecha_inicio(Fecha);
            int mont = Fecha.getMonth() + 1;
            Fecha.setMonth(mont);
            ea.setFecha_fin(Fecha);
            e.create(ea);
    }
    
    public static void confirm(){
        try {
            Implements_ER imp= new Implements_ER();
            if(imp.showAll().size()<1){
                Estado_Result ea = new Estado_Result();
                Date Fecha = new Date();
                ea.setVenta(0);
                ea.setCosto_venta(0);
                ea.setGasto_gen(0);
                ea.setDividendos_comunes(0);
                ea.setDividendos_comunes(0);
                ea.setImp_venta(0);
                ea.setFecha_inicio(Fecha);
                int mont = Fecha.getMonth() + 1;
                Fecha.setMonth(mont);
                ea.setFecha_fin(Fecha);
                imp.create(ea);
                
            }
            Implements_compra impl= new  Implements_compra();
            if(impl.showAlli().size()<1){
                COmpra co= new COmpra();
                co.setDinero(0.00);
                impl.create(co);
            }
        } catch (IOException ex) {
            Logger.getLogger(actualizaciones_archivos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
