
package Cont.clases.razones;

import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_ER;
import DAO.Implements_balance;
import java.io.IOException;
import java.util.List;

public class RazonesFinancieras {
    
      
   public static String capitaldetrabajo() throws IOException {
        double activoCirculanteT = 0;
        double pasivoCirculanteT = 0;
        Implements_balance imp = new Implements_balance();
        List<Cuentas_balance> cuentas = imp.showAll();
        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo") && cu.getTiempo().equals("circulante")) {
                activoCirculanteT = activoCirculanteT + cu.getDinero();
            } else if (cu.getTipo().equals("pasivo") && cu.getTiempo().equals("circulante")) {
                pasivoCirculanteT = pasivoCirculanteT + cu.getDinero();
            }

        }

        double capitalTrabajo = activoCirculanteT - pasivoCirculanteT;
        String s = "";
        if (capitalTrabajo < 0) {
            s = "La empresa actualmente refleja una capital de trabajo:" + capitalTrabajo + " lo que significa que la empresa no esta generando liquidez";
        }
        if (capitalTrabajo > 0) {
            s = "actualmente el capital de trabajo es" + capitalTrabajo + "lo que significa que la empresa genera liquidez en lo que se refiere al control interno de la empresa";
        }
        return s;
    }

    public static String indice_de_solvencia(double Promedio_Ind) throws IOException/*(float activoCirculanteT,float pasivoCirculanteT)*/ {
        double activoCirculanteT = 0;
        double pasivoCirculanteT = 0;
        Implements_balance imp = new Implements_balance();
        List<Cuentas_balance> cuentas = imp.showAll();

        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo") && cu.getTiempo().equals("circulante")) {
                activoCirculanteT = activoCirculanteT + cu.getDinero();
            } else if (cu.getTipo().equals("pasivo") && cu.getTiempo().equals("circulante")) {
                pasivoCirculanteT = pasivoCirculanteT + cu.getDinero();
            }

        }

        double indiceSolvencia = activoCirculanteT / pasivoCirculanteT;

        String s = "";

        if (indiceSolvencia < Promedio_Ind) {

            s = "La razon circulante de la empresa" + indiceSolvencia
                    + "es inferior al promedio de la industria que es " + Promedio_Ind + " lo que signidica que su posicion de liquidez es debil\n "
                    + " Sin embargo, toda vez que se ha programado que los activos circulantes se conviertan en efectivo en el futuro\n "
                    + "cercano es altamente probable que puedan liquidarse a un precio muy cercano a su valor estipulado.";
        }
        if (indiceSolvencia > Promedio_Ind) {
            s = "La razon circulante de la empresa" + indiceSolvencia + "es superior al promedio de la industria que es\n  " + Promedio_Ind
                    + " lo que signidica que su posicion de liquidez es bueno y podra cubrir sus obligaciones";
        }

        return s;
    }

    public static String pruebaAcida(double inventarios, double prom_industriapa) throws IOException {
        double activoCirculanteT = 0;
        double pasivoCirculanteT = 0;

        Implements_balance imp = new Implements_balance();

        List<Cuentas_balance> cuentas = imp.showAll();
        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo") && cu.getTiempo().equals("circulante")) {
                activoCirculanteT = activoCirculanteT + cu.getDinero();
            } else if (cu.getTipo().equals("pasivo") && cu.getTiempo().equals("circulante")) {
                pasivoCirculanteT = pasivoCirculanteT + cu.getDinero();
            }

        }

        double pruebaAcida = (activoCirculanteT - inventarios) / pasivoCirculanteT;
        //(activoCirculante -inventarios ) / pasivoCirculante

        String s = "";

        if (pruebaAcida < prom_industriapa) {

            s = "El promedio industrial correspondiente a la razon rapida es" + prom_industriapa
                    + "\npor lo que el valor de la razon de la empresa " + pruebaAcida
                    + " es bajo en comparacion con \nlas razones de otras empresas de su industria";
        }
        if (pruebaAcida > prom_industriapa) {
            s = "El promedio industrial correspondiente a la razon rapida es\n" + prom_industriapa
                    + "por lo que el valor de la razon de la empresa \n" + pruebaAcida
                    + "es mayor en comparacion con las razones de otrs empresas de su industria";
        }

        return s;
    }

    //...................Razones De  Actividad.......................................................................  
    public static String RotacionDeInventario(double inventarios, double prom_industriari) throws IOException {

        Implements_ER impl = new Implements_ER();
        Estado_Result e = impl.showAll().get(0);

        e.getCosto_venta();

        double rotacionInventarios = e.getCosto_venta() / inventarios;
        //CtoArtVendidos / inventarios 
        String s = "";

        if (rotacionInventarios < prom_industriari) {

            s = "Como aproximación preliminar, cada uno de los artículos del inventario de la empresa"
                    + " se vende nuevamente y se almacena o rota\n " + rotacionInventarios
                    + "veces por mes, esto es considerablemente bajo que el promedio industrial de \n" + prom_industriari
                    + " veces";
        }
        if (rotacionInventarios > prom_industriari) {
            s = "Como aproximación preliminar, cada uno de los artículos del inventario de la empresa\n"
                    + " se vende nuevamente y se almacena o rota \n" + rotacionInventarios
                    + "veces por año, esto es mayor que el promedio industrial de " + prom_industriari
                    + "veces";
        }

        return s;

    }

    public static String RotacionDeCtasxCobrar(double VtasAlCredito, double cuentasxCobNetas) throws IOException {//////////////CONCLUSION,NOHAYEJMEPLO///////////////////

        double rotacionCtasxCob = VtasAlCredito / cuentasxCobNetas;
        //VtasAlCredito / cuentasxCobNetas
        String s = "";

        /* if( RotacionDeCtasxCobrar <prom_industriari){
    
        s="Como aproximación preliminar, cada uno de los artículos del inventario de la empresa"
                + " se vende nuevamente y se almacena o rota "+rotacionInventarios
                + "veces por año, esto es considerablemente bajo que el promedio industrial de "+prom_industriari
                +" veces";
    }if(RotacionDeCtasxCobrar>prom_industriari){
        s="Como aproximación preliminar, cada uno de los artículos del inventario de la empresa"
                + " se vende nuevamente y se almacena o rota "+rotacionInventarios
                +"veces por año, esto es mayor que el promedio industrial de "+prom_industriari
                +"veces";*
    } */
        return s;
    }

    public static String PeriodoPromedioCobro(double Ventas, double cuentasxCobrar) throws IOException {

        double periodoPromedioCob = cuentasxCobrar / (Ventas/*VtasAnuales*/ / 60);
        //cuentasxCobNetas / (Ventas/*VtasAnuales*/ / 360)
        String s = "";

        s = "En  promedio la empresa requiere " + periodoPromedioCob + "días para recaudar una cuenta "
                + "por cobrar. \n El período de cobro es significativo solo en relación con  las condiciones de crédito de la empresa";

        return s;
    }

    public static String RotacionDeActFijo(double Ventas, double prom_industriaraf) throws IOException {

        double activoFijoT = 0;

        Implements_balance imp = new Implements_balance();
        List<Cuentas_balance> cuentas = imp.showAll();

        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo") && cu.getFinali().equals("fijo"));
            {
                activoFijoT = activoFijoT + cu.getDinero();
            }

        }

        double RotacionActFijo = Ventas / activoFijoT;
        //Ventas / ActivosFijosNetos
        String s = "";

        if (RotacionActFijo < prom_industriaraf) {

            s = "Si esta razón " + RotacionActFijo + "no se contrasta con un promedio de industria de\n " + prom_industriaraf
                    + "veces significa que la empresa no utiliza sus activos fijos casi tan intensamente \n"
                    + "como las demás. Significa que la empresa puede  tener  demasiados activos\n "
                    + "o muy pocos de ellos en relación con otras empresas.";

        }
        if (RotacionActFijo > prom_industriaraf) {
            s = "Si esta razón " + RotacionActFijo + "se contrasta con un promedio de industria de \n" + prom_industriaraf
                    + "veces significa que la empresa utiliza sus activos fijos casi tan intensamente\n "
                    + "como las demás. Significa que la empresa no parece tener ni demasiados activos \n"
                    + "ni muy pocos de ellos en relación con otras empresas.";
        }

        return s;
    }

    public static String RotacionDeActivosTotales(double Ventas) throws IOException {

        double activo = 0;

        Implements_balance imp = new Implements_balance();
        List<Cuentas_balance> cuentas = imp.showAll();

        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo")) {
                activo = activo + cu.getDinero();
            }

        }

        double RotacionActTotales = Ventas / activo;
        //Ventas / ActivosTotales
        String s = "";

        s = "Esto significa que la empresa renueva sus activos" + RotacionActTotales + " veces al año.";

        return s;
    }

    //...............Razones De Endeudamiento................................................................... 
    public static String RazonDeDeudaTotal() throws IOException {

        double activo = 0;
        double pasivo = 0;

        Implements_balance imp = new Implements_balance();

        List<Cuentas_balance> cuentas = imp.showAll();
        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo")) {
                activo = activo + cu.getDinero();
            } else if (cu.getTipo().equals("pasivo")) {
                pasivo = pasivo + cu.getDinero();
            }

        }

        double razonDeuda = (pasivo / activo) * 100;
        String s = "";

        s = "Esto indica que la empresa financió la" + razonDeuda + "% de sus activos por medio del endeudamiento";

        return s;
    }

    public static String RazonPasivoaCapital() throws IOException {

        double capitalSocial = 0;
        double pasivoNoCirculanteT = 0;

        Implements_balance imp = new Implements_balance();

        List<Cuentas_balance> cuentas = imp.showAll();
        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("capital") ) {
                capitalSocial += cu.getDinero();
            } else if (cu.getTipo().equals("pasivo") && cu.getTiempo().equals("no_circulante")) {
                pasivoNoCirculanteT +=  cu.getDinero();
            }

        }

        double razonPasivoaCapital = pasivoNoCirculanteT / capitalSocial;
        //pasivo_LP / capital_social
        String s = "";

        s = " la relacion de los fondos suministrados es de " + razonPasivoaCapital + "% \npor acredores y apotados por los propietarios de la empresa ";

        return s;
    }

    public static String RazonDeRotacionDeInteresaUtilidades(double Utilidad_AntesdeInteimp, double cargosxinteres) throws IOException {
        Implements_ER impl = new Implements_ER();
        Estado_Result e = impl.showAll().get(0);

        //  e.Utilidad_AntesdeInteimp();
        double rotacionInteresaUtilidad = Utilidad_AntesdeInteimp / cargosxinteres;
        //Utilidad_AntesdeInteimp / cargosxinteres;
        String s = "";

        if (rotacionInteresaUtilidad < 3) {

            s = "La razon de capacidad de pago es de intereses de la empresa es " + rotacionInteresaUtilidad + "parece aceptable\n"
                    + "el riesgo tanto para los prestamistas como\n "
                    + "para los propietarios es bajo ";

        }
        if (rotacionInteresaUtilidad > 3) {

            s = "La razon de capacidad de pago es de intereses de la empresa es " + rotacionInteresaUtilidad + "\n,es bajapor ende es"
                    + "mayor el riesgo tanto para los prestamistas comon\n"
                    + "para los propietarios";

        }

        return s;

    }

    //................Razones De Rentabilidad....................................................................... 
    public static String MargenUtilidadBruta(double ctoArticulovendido, double Ventas) throws IOException {

        /*Implements_ER impl =new Implements_ER();
            Estado_Result e= impl.showAll().get(0);
            
           e.ctoArticulovendido();*/
        double MargenUtlBruta = (Ventas - ctoArticulovendido) / Ventas;
        //(Ventas - CtoVtas) / Ventas

        String s = "El margen de utilida bruta es " + MargenUtlBruta + "Indica que por cada dólar de venta se obtiene\n "
                + "un MUB de" + MargenUtlBruta + " centavos de dólar";

        return s;
    }

    public static String MargeUtilidadOperativa(double UtlOperativa, double Ventas) throws IOException {

        double MargenUtOperativa = UtlOperativa / Ventas;
        //UtlOperativa / Ventas

        String s = "La utilidad de operación representa " + MargenUtOperativa + "%  de las ventas; \n"
                + "es decir" + MargenUtOperativa + "\n centavos por cada dólar en venta.";
        return s;
    }

    public static String MargenUtilidadNeta(double UtlNetaDespDeImp, double Ventas) {

        double MargenUtlNeta = UtlNetaDespDeImp / Ventas;
        // UtlNetaDespDeImp / Ventas

        String s = "";
        s = "El margen de utilidad neta es de" + MargenUtlNeta + "centavos por cada dólar en venta.";

        return s;
    }

    public static String RendimientosActivos(double UtlNetaDespDeImp) throws IOException {
        double activoT = 0;

        Implements_balance imp = new Implements_balance();
        List<Cuentas_balance> cuentas = imp.showAll();

        for (Cuentas_balance cu : cuentas) {
            if (cu.getTipo().equals("Activo")) {
                activoT = activoT + cu.getDinero();
            }

        }

        double RendimientosActivos = UtlNetaDespDeImp / activoT;
        String s = "";
        s = "El rendimiento de los activos es" + RendimientosActivos + "%";

        return s;
    }

     
  
    
}
    
    
    
    
    
   