
package Cont.clases.BG.ER;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Estado_Result {
    int id;
    double venta;
    double costo_venta;
    double gasto_gen;
    double imp_venta;
    double dividendos_preferentes;
    double dividendos_comunes;
    Date fecha_inicio;
    Date fecha_fin;

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    

    public void setVenta(double venta) {
        this.venta = venta;
    }

    public void setCosto_venta(double costo_venta) {
        this.costo_venta = costo_venta;
    }

    public double getDividendos_preferentes() {
        return dividendos_preferentes;
    }

    public double getDividendos_comunes() {
        return dividendos_comunes;
    }

    public void setDividendos_preferentes(double dividendos_preferentes) {
        this.dividendos_preferentes = dividendos_preferentes;
    }

    public void setDividendos_comunes(double dividendos_comunes) {
        this.dividendos_comunes = dividendos_comunes;
    }

    

   

    public void setGasto_gen(double gasto_gen) {
        this.gasto_gen = gasto_gen;
    }

    public void setImp_venta(double imp_venta) {
        this.imp_venta = imp_venta;
    }

   

    public double getVenta() {
        return venta;
    }

    public double getCosto_venta() {
        return costo_venta;
    }

    

    public double getGasto_gen() {
        return gasto_gen;
    }

    public double getImp_venta() {
        return imp_venta;
    }

    
    
    public List<String> toList(){
        List<String> empleado = new ArrayList<>();
        
        empleado.add(String.valueOf(id));
        empleado.add(String.valueOf(venta));
        empleado.add(String.valueOf(costo_venta));
        empleado.add(String.valueOf(gasto_gen));
        empleado.add(String.valueOf(imp_venta));
        empleado.add(String.valueOf(dividendos_preferentes));
        empleado.add(String.valueOf(dividendos_comunes));
        empleado.add(fecha_inicio.toString());
        empleado.add(fecha_fin.toString());
        
        return empleado;
    }
    
}
