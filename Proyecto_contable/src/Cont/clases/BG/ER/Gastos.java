package Cont.clases.BG.ER;

import java.util.Date;

public class Gastos {

    int id;
    String Nombre;
    Date Fecha_Incio; //
    Date UltimaFechaCobrada;
    Date Fecha_Final; //
    int periodo;
    double[] interesMensual;
    double[] pagoPrincipal;
    double mensualidad;
    //int dias;
    boolean Active;

    /*public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getNombre() {
        return Nombre;
    }

    public Date getFecha_Incio() {
        return Fecha_Incio;
    }

    public Date getFecha_Final() {
        return Fecha_Final;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean Active) {
        this.Active = Active;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void setFecha_Incio(Date Fecha_Incio) {
        this.Fecha_Incio = Fecha_Incio;
    }

    public Date getUltimaFechaCobrada() {
        return UltimaFechaCobrada;
    }

    public void setUltimaFechaCobrada(Date UltimaFechaCobrada) {
        this.UltimaFechaCobrada = UltimaFechaCobrada;
    }

    public void setFecha_Final(Date Fecha_Final) {
        this.Fecha_Final = Fecha_Final;
    }

    public double[] getInteresMensual() {
        return interesMensual;
    }

    public void setInteresMensual(double[] interesMensual) {
        this.interesMensual = interesMensual;
    }

    public double[] getPagoPrincipal() {
        return pagoPrincipal;
    }

    public void setPagoPrincipal(double[] pagoPrincipal) {
        this.pagoPrincipal = pagoPrincipal;
    }

    public double getMensualidad() {
        return mensualidad;
    }

    public void setMensualidad(double mensualidad) {
        this.mensualidad = mensualidad;
    }

    public Gastos(int id, String Nombre, Date Fecha_Incio, Date UltimaFechaCobrada, Date Fecha_Final, double[] interesMensual, double[] pagoPrincipal, double mensualidad, int periodo, boolean Active) {
        this.id = id;
        this.Nombre = Nombre;
        this.Fecha_Incio = Fecha_Incio;
        this.UltimaFechaCobrada = UltimaFechaCobrada;
        this.Fecha_Final = Fecha_Final;
        this.interesMensual = interesMensual;
        this.pagoPrincipal = pagoPrincipal;
        this.mensualidad = mensualidad;
        this.periodo = periodo;
        this.Active = Active;
    }

    @Override
    public String toString() {
        return "Gastos{" + "id=" + id + ", Nombre=" + Nombre + ", Fecha_Incio=" + Fecha_Incio + ", UltimaFechaCobrada=" + UltimaFechaCobrada + ", Fecha_Final=" + Fecha_Final + ", periodo=" + periodo + ", mensualidad=" + mensualidad + ", Active=" + Active + '}';
    }

    public Gastos() {
    }

}
