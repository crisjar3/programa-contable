/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cont.clases.BG.ER;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Comprobantes {
    int id;
    String Nombre;
    String cuenta_afecta;
    String cuenta_auxiliar;
    String estado;
    String Descrpcion;
    double cantidad_deb;
    double cantidad_hab;
    String CuentaER_afect;
    double Cantidad_ER;
    Date fecha;

    public double getCantidad_ER() {
        return Cantidad_ER;
    }

    public void setCantidad_ER(double Cantidad_ER) {
        this.Cantidad_ER = Cantidad_ER;
    }

    
    public Date getFecha() {
        return fecha;
    }

    public void setCuentaER_afect(String CuentaER_afect) {
        this.CuentaER_afect = CuentaER_afect;
    }

    public String getCuentaER_afect() {
        return CuentaER_afect;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setDescrpcion(String Descrpcion) {
        this.Descrpcion = Descrpcion;
    }

    public String getEstado() {
        return estado;
    }

    public String getDescrpcion() {
        return Descrpcion;
    }

   

    public String getCuenta_afecta() {
        return cuenta_afecta;
    }

    public String getCuenta_auxiliar() {
        return cuenta_auxiliar;
    }

    public void setCuenta_afecta(String cuenta_afecta) {
        this.cuenta_afecta = cuenta_afecta;
    }

    public void setCuenta_auxiliar(String cuenta_auxiliar) {
        this.cuenta_auxiliar = cuenta_auxiliar;
    }

    public double getCantidad_deb() {
        return cantidad_deb;
    }

    public double getCantidad_hab() {
        return cantidad_hab;
    }

    public void setCantidad_deb(double cantidad_deb) {
        this.cantidad_deb = cantidad_deb;
    }

    public void setCantidad_hab(double cantidad_hab) {
        this.cantidad_hab = cantidad_hab;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
   
    public Comprobantes() {
    }

    public String getNombre() {
        return Nombre;
    }

    

    
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public List<String> toList(){
        List<String> empleado = new ArrayList<>();
        
        empleado.add(String.valueOf(id));
        empleado.add(Nombre);
        empleado.add(estado);
        empleado.add(Descrpcion);
        empleado.add(cuenta_afecta);
        empleado.add(cuenta_auxiliar);
        empleado.add(String.valueOf(cantidad_deb));
        empleado.add(String.valueOf(cantidad_hab));
        empleado.add(CuentaER_afect);
        empleado.add( String.valueOf(Cantidad_ER));
        empleado.add(fecha.toString());
        
        return empleado;
        
    }
  

    
    
    
    
}
