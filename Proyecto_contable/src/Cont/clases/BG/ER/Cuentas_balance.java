/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cont.clases.BG.ER;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cristhian Joel Gomez
 */
public class Cuentas_balance{
    int Id_cuenta;
    String Nombre_cuenta;
    String tipo;
    String tiempo;
    String finali;
    double Dinero;
    double dinero_inicial;
    

    public Cuentas_balance() {
    }

    public Cuentas_balance(int Id_cuenta, String Nombre_cuenta, String tipo, String tiempo, String finali, double Dinero, double dinero_inicial) {
        this.Id_cuenta = Id_cuenta;
        this.Nombre_cuenta = Nombre_cuenta;
        this.tipo = tipo;
        this.tiempo = tiempo;
        this.finali = finali;
        this.Dinero = Dinero;
        this.dinero_inicial = dinero_inicial;
    }

    public double getDinero_inicial() {
        return dinero_inicial;
    }

    public void setDinero_inicial(double dinero_inicial) {
        this.dinero_inicial = dinero_inicial;
    }

  

   

    public double getDinero() {
        return Dinero;
    }

    public void setDinero(double Dinero) {
        this.Dinero = Dinero;
    }

   
    
   
    public int getId_cuenta() {
        return Id_cuenta;
    }

    public String getNombre_cuenta() {
        return Nombre_cuenta;
    }

    public String getFinali() {
        return finali;
    }

    public void setFinali(String finali) {
        this.finali = finali;
    }

  

    

   

    public String getTipo() {
        return tipo;
    }

    public void setId_cuenta(int Id_cuenta) {
        this.Id_cuenta = Id_cuenta;
    }

    public void setNombre_cuenta(String Nombre_cuenta) {
        this.Nombre_cuenta = Nombre_cuenta;
    }

    

    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public String toString() {
        return "Cuentas_balance{" + "Id_cuenta=" + Id_cuenta + ", Nombre_cuenta=" + Nombre_cuenta + ", tipo=" + tipo + ", tiempo=" + tiempo + ", finali=" + finali + ", Dinero=" + Dinero + '}';
    }
     public List<String> toList(){
        List<String> empleado = new ArrayList<>();
        
        empleado.add(String.valueOf(Id_cuenta));
        empleado.add(Nombre_cuenta);
        empleado.add(tipo);
        empleado.add(tiempo);
        empleado.add(finali);
        empleado.add(String.valueOf(Dinero));
        empleado.add(String.valueOf(dinero_inicial));
        return empleado;
        
    }
    
}
