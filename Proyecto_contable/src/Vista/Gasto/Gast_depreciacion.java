package Vista.Gasto;

import Cont.clases.BG.ER.Gastos;
import DAO.Implements_Comprobantes;
import DAO.Implements_balance;
import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Cuentas_balance;
import DAO.Implements_gastos;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class Gast_depreciacion extends javax.swing.JDialog {

    Implements_balance impBG;
    Implements_Comprobantes impComprob;
    Implements_gastos impGast;
    String[] cuentaP = {"MAQUINARIA"};
    String[] cuentaS = {"BANCO"};

    public Gast_depreciacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        impBG = new Implements_balance();
        impComprob = new Implements_Comprobantes();
        impGast = new Implements_gastos();
        cmbCuentaPrincipal.setModel(new DefaultComboBoxModel(cuentaP));
        cmbCuentaAuxiliar.setModel(new DefaultComboBoxModel(cuentaS));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtFNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtADescrip = new javax.swing.JTextArea();
        cmbCuentaPrincipal = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        cmbCuentaAuxiliar = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnGuardarMaquina = new javax.swing.JButton();
        ftxtFVidaUtil = new javax.swing.JFormattedTextField();
        ftxtFValorSalvamento = new javax.swing.JFormattedTextField();
        ftxtFDinero = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel3.setText("Cuenta Principal Afectada:");

        jLabel4.setText("Cuenta Auxiliar Afectada:");

        txtADescrip.setColumns(20);
        txtADescrip.setRows(5);
        jScrollPane1.setViewportView(txtADescrip);

        cmbCuentaPrincipal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        cmbCuentaPrincipal.setEnabled(false);

        jLabel7.setText("Dinero:");

        cmbCuentaAuxiliar.setEnabled(false);

        jLabel1.setText("Nombre de Comprobantes:");

        jLabel2.setText("Descripcion:");

        jLabel6.setText("Vida útil (Años):");

        jLabel5.setText("Valor de salvamento:");

        btnGuardarMaquina.setText("Guardar");
        btnGuardarMaquina.setName("btnGuardarMaquina"); // NOI18N
        btnGuardarMaquina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarMaquinaActionPerformed(evt);
            }
        });

        ftxtFVidaUtil.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        ftxtFVidaUtil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ftxtFVidaUtilKeyTyped(evt);
            }
        });

        ftxtFValorSalvamento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        ftxtFValorSalvamento.setToolTipText("");
        ftxtFValorSalvamento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ftxtFValorSalvamentoKeyTyped(evt);
            }
        });

        ftxtFDinero.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        ftxtFDinero.setToolTipText("");
        ftxtFDinero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ftxtFDineroKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(106, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(btnGuardarMaquina, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ftxtFVidaUtil, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ftxtFValorSalvamento, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbCuentaPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbCuentaAuxiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(ftxtFDinero, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtFNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtFNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jLabel2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbCuentaPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbCuentaAuxiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ftxtFDinero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ftxtFVidaUtil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ftxtFValorSalvamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addComponent(btnGuardarMaquina)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarMaquinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarMaquinaActionPerformed
        try {
            if (comprobar()) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                double depreciacion = depreciacionMensual(Double.parseDouble(ftxtFDinero.getText()), Double.parseDouble(ftxtFValorSalvamento.getText()),
                        Integer.parseInt(ftxtFVidaUtil.getText()));

                Gastos gasto = new Gastos();
                gasto.setNombre("Depreciacion");
                gasto.setFecha_Incio(new Date());

                gasto.setUltimaFechaCobrada(new Date());

                Calendar fin = Calendar.getInstance();
                fin.setTime(gasto.getFecha_Incio());
                fin.add(Calendar.YEAR, Integer.parseInt(ftxtFVidaUtil.getText()));

                gasto.setFecha_Final(fin.getTime());

                long periodoMeses = ChronoUnit.MONTHS.between(LocalDate.parse(formatter.format(gasto.getFecha_Incio())).atStartOfDay(),
                        LocalDate.parse(formatter.format(gasto.getFecha_Final())).atStartOfDay());

                gasto.setPeriodo(Math.toIntExact(periodoMeses));
                gasto.setMensualidad(depreciacion);
                gasto.setActive(true);

                JOptionPane.showMessageDialog(this, "Desde Gast_depreciacion " + gasto.toString());

                impGast.create(gasto);

                Comprobantes maquinaria = new Comprobantes();

                maquinaria.setNombre(txtFNombre.getText());
                maquinaria.setCuenta_afecta(cmbCuentaPrincipal.getItemAt(0));
                maquinaria.setCuenta_auxiliar(cmbCuentaAuxiliar.getItemAt(0));
                maquinaria.setEstado("Debe");
                maquinaria.setDescrpcion(txtADescrip.getText());
                maquinaria.setCantidad_deb(Double.parseDouble(ftxtFDinero.getText()));
                maquinaria.setCantidad_hab(Double.parseDouble(ftxtFDinero.getText()));
                maquinaria.setCuentaER_afect("Depreciacion");
                maquinaria.setCantidad_ER(depreciacion);
                maquinaria.setFecha(new Date());

                List<Cuentas_balance> cuentaMaquin = impBG.showAll().stream().filter(c -> (c.getNombre_cuenta().equals("MAQUINARIA"))).collect(Collectors.toList());
                Cuentas_balance cuentaBGMaqu = cuentaMaquin.get(0);
                cuentaBGMaqu.setDinero(cuentaBGMaqu.getDinero() + Double.parseDouble(ftxtFDinero.getText()));

                List<Cuentas_balance> cuentaBan = impBG.showAll().stream().filter(c -> (c.getNombre_cuenta().equals("BANCO"))).collect(Collectors.toList());
                Cuentas_balance cuentaBGBan = cuentaBan.get(0);
                cuentaBGBan.setDinero(cuentaBGBan.getDinero() - Double.parseDouble(ftxtFDinero.getText()));

                impComprob.create(maquinaria);
                impBG.update(cuentaBGMaqu);
                impBG.update(cuentaBGBan);

                clear();
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Gast_depreciacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnGuardarMaquinaActionPerformed

    private void ftxtFValorSalvamentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxtFValorSalvamentoKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.') {
            evt.consume();
        }
        if (evt.getKeyChar() == '.' && ftxtFValorSalvamento.getText().contains(".")) {
            evt.consume();
        }
    }//GEN-LAST:event_ftxtFValorSalvamentoKeyTyped

    private void ftxtFVidaUtilKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxtFVidaUtilKeyTyped
        if (!Character.isDigit(evt.getKeyChar())) {
            evt.consume();
        }
    }//GEN-LAST:event_ftxtFVidaUtilKeyTyped

    private void ftxtFDineroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxtFDineroKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.') {
            evt.consume();
        }
        if (evt.getKeyChar() == '.' && ftxtFDinero.getText().contains(".")) {
            evt.consume();
        }
    }//GEN-LAST:event_ftxtFDineroKeyTyped

    private double depreciacionMensual(double inicial, double valorSalvamento, int vidaUtil) {
        double depreciacionMensual = 0;

        depreciacionMensual = (double) ((inicial - valorSalvamento) / vidaUtil) / 12;
        return (double) Math.round(depreciacionMensual * 100) / 100;
    }

    private void clear() {
        txtADescrip.setText("");
        ftxtFDinero.setText("");
        txtFNombre.setText("");
        ftxtFValorSalvamento.setText("");
        ftxtFVidaUtil.setText("");
    }

    private boolean comprobar() {
        if (txtADescrip.getText().trim().equals("") || ftxtFDinero.getText().trim().equals("")
                || txtFNombre.getText().trim().equals("") || ftxtFValorSalvamento.getText().trim().equals("")
                || ftxtFVidaUtil.getText().trim().equals("")) {
            JOptionPane.showConfirmDialog(this, "No puede dejar campos vacios", "Mensaje de error", JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (Double.parseDouble(ftxtFVidaUtil.getText()) > 20) {
            JOptionPane.showConfirmDialog(this, "La vida util no puede ser mayor a 20 años", "Mensaje de error", JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gast_depreciacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gast_depreciacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gast_depreciacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gast_depreciacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Gast_depreciacion dialog = new Gast_depreciacion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarMaquina;
    private javax.swing.JComboBox<String> cmbCuentaAuxiliar;
    private javax.swing.JComboBox<String> cmbCuentaPrincipal;
    private javax.swing.JFormattedTextField ftxtFDinero;
    private javax.swing.JFormattedTextField ftxtFValorSalvamento;
    private javax.swing.JFormattedTextField ftxtFVidaUtil;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtADescrip;
    private javax.swing.JTextField txtFNombre;
    // End of variables declaration//GEN-END:variables
}
