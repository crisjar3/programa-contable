/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.PronosticoCaja;

import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Gastos;
import DAO.Implements_balance;
import DAO.Implements_gastos;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class PronosticoCaja extends javax.swing.JDialog {

    
    public PronosticoCaja(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        txtConclusiones.setEditable(false);
        txtGastosFijos.setEditable(false);
        txtPorpagar.setEditable(false);
        txtSaldoInicial.setEditable(false);
        txtSaldoMinimo.setEditable(false);
        txtTotal.setEnabled(false);
        txtVent.setEnabled(false);
        txtVent.setText(String.valueOf(Pronostico.Pronostico_ventas.prono(2)));
        generar();
        
        txtTotal.setEnabled(false);
    }
    public void generar(){
        try {
            
            Implements_gastos impgast= new Implements_gastos();
            Implements_balance impbal = new Implements_balance();
            List<Cuentas_balance> listCuent= impbal.showAll();
            List<Gastos> ListGast= impgast.showAll();
            double gast= 0.00;
            double total=00;
            
            for(Cuentas_balance e: listCuent){
                if(e.getNombre_cuenta().equals("CAJA")){
                    
                    txtSaldoInicial.setText(String.valueOf( e.getDinero_inicial()));
                }if(e.getNombre_cuenta().equals("CUENTAS POR COBRAR")){
                    txtPorpagar.setText(String.valueOf(e.getDinero()));
                }
            }
            for(Gastos e :ListGast){
                gast=e.getMensualidad();
            }
            txtPorpagar.setText("345");
            txtGastosFijos.setText(String.valueOf(gast));
           total=Pronostico.Pronostico_ventas.prono(1)+Double.parseDouble(txtPorpagar.getText())-Double.parseDouble(txtGastosFijos.getText())
                    +Double.parseDouble(txtSaldoInicial.getText())-Double.parseDouble(txtSaldoMinimo.getText());
            txtTotal.setText(String.valueOf(total));
            if(total>0){
                txtConclusiones.setText("Actualmente el pronostico de caja no presenta necesidad de finanaciamiento \n ya que es posi"
                        + "tivo para el siguiente mes ");
            }else {
                txtConclusiones.setText("actualmente el pronostico dicta que hay una deficiencia \ny se necesita una"
                        + " financiacion");
            }
        } catch (IOException ex) {
            Logger.getLogger(PronosticoCaja.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtPorpagar = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSaldoMinimo = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtGastosFijos = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtSaldoInicial = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtConclusiones = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtVent = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Ventas Pronosticadas");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(51, 25, -1, -1));
        getContentPane().add(txtPorpagar, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 60, 110, -1));

        jLabel2.setText("Cuentas por cobrar a un mes:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        jLabel3.setText("Conclusiones:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, -1, -1));

        txtSaldoMinimo.setText("1000");
        getContentPane().add(txtSaldoMinimo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 110, -1));
        getContentPane().add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 40, 110, -1));

        jLabel4.setText("Gatos Fijos a pagar:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(51, 98, -1, -1));
        getContentPane().add(txtGastosFijos, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 110, -1));

        jLabel5.setText("Saldo inicial de caja:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, -1));
        getContentPane().add(txtSaldoInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 130, 110, -1));

        txtConclusiones.setColumns(20);
        txtConclusiones.setRows(5);
        jScrollPane1.setViewportView(txtConclusiones);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 270, 360, -1));

        jLabel6.setText("Saldo minimo:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 180, -1, -1));

        btnSalir.setText("Ok");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 210, -1, -1));

        jLabel7.setText("Total");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 40, -1, -1));
        getContentPane().add(txtVent, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 20, 110, -1));

        setSize(new java.awt.Dimension(492, 443));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        this.dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_btnSalirActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PronosticoCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PronosticoCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PronosticoCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PronosticoCaja.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PronosticoCaja dialog = new PronosticoCaja(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtConclusiones;
    private javax.swing.JTextField txtGastosFijos;
    private javax.swing.JTextField txtPorpagar;
    private javax.swing.JTextField txtSaldoInicial;
    private javax.swing.JTextField txtSaldoMinimo;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtVent;
    // End of variables declaration//GEN-END:variables
}
