/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.Comprobante;

import Cont.clases.BG.ER.Comprobantes;
import DAO.Implements_Comprobantes;
import Vista.BG.Admin_cuentas_balance;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import model.Tabmod_comprobantes;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Cristhian Joel Gomez
 */
public class Admin_COmprobantes extends javax.swing.JDialog {

    Tabmod_comprobantes model_BG; 
    Implements_Comprobantes imp= new Implements_Comprobantes();
    public Admin_COmprobantes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        load();
    }

   public void load(){
        model_BG=new Tabmod_comprobantes();
         try {
            model_BG.loadFromFile();
        } catch (IOException ex) {
            Logger.getLogger(Admin_cuentas_balance.class.getName()).log(Level.SEVERE, null, ex);
        }
        tblEstudiantes.setModel(model_BG);
        pnlOK.setVisible(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlOK = new javax.swing.JPanel();
        btnOK = new javax.swing.JButton();
        pnlBoton = new javax.swing.JPanel();
        panel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEstudiantes = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });
        pnlOK.add(btnOK);

        getContentPane().add(pnlOK, java.awt.BorderLayout.SOUTH);

        pnlBoton.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        getContentPane().add(pnlBoton, java.awt.BorderLayout.NORTH);

        panel.setLayout(new java.awt.BorderLayout());

        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseClicked(evt);
            }
        });

        tblEstudiantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Apellido", "Nota"
            }
        ));
        tblEstudiantes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEstudiantesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblEstudiantes);

        panel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(732, 337));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        panel.removeAll();
        pnlOK.setVisible(false);
        panel.add(jScrollPane1);
      
    }//GEN-LAST:event_btnOKActionPerformed

    public int id_rep(){
        int o= tblEstudiantes.getSelectedRow();
        
        
        return o;
    }
    public void report(int i){
         try {
            List<Comprobantes> comprobantes = new ArrayList<>();
            comprobantes.add(imp.showAll().get(i));
            
            JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(comprobantes);

            Map<String, Object> params = new HashMap<>();

            JasperReport jasperReport= (JasperReport) JRLoader.loadObjectFromFile("src/resources/Comprobantes.jasper");

            InputStream imgInputStream = this.getClass().getResourceAsStream("/resources/logo.png");
            
            params.put("logo", imgInputStream);
            
            JasperPrint jrPrint;
            jrPrint = JasperFillManager.
            fillReport(jasperReport, params, datasource);

            
            panel.remove(jScrollPane1);
            panel.add(new JRViewer(jrPrint));
            this.setSize(this.getToolkit().getScreenSize());
            pnlOK.setVisible(true);
        } catch (IOException | JRException ex) {
            Logger.getLogger(Admin_cuentas_balance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void jScrollPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseClicked
        
    }//GEN-LAST:event_jScrollPane1MouseClicked

    private void tblEstudiantesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEstudiantesMouseClicked

      if(evt.getClickCount()==2){
          
         report(id_rep());
       }
    }//GEN-LAST:event_tblEstudiantesMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin_COmprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin_COmprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin_COmprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin_COmprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Admin_COmprobantes dialog = new Admin_COmprobantes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOK;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel;
    private javax.swing.JPanel pnlBoton;
    private javax.swing.JPanel pnlOK;
    private javax.swing.JTable tblEstudiantes;
    // End of variables declaration//GEN-END:variables
}
