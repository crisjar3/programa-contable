
package Vista.Comprobante;

import Cont.clases.BG.ER.COmpra;
import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_Comprobantes;
import DAO.Implements_ER;
import DAO.Implements_balance;
import DAO.Implements_compra;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;


public final class jAdd_comprobantes extends javax.swing.JDialog {

     enum deb{
        Debe,Haber
    }
     
    Implements_balance imp;
    Date fecha;
    
    boolean Cost= false;
    boolean Depre= false;
    boolean Interes= false;
    
    public jAdd_comprobantes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        
        initComponents();
         
        cmbCuenta_aux.setVisible(false);
        cmbEstado.setModel(new DefaultComboBoxModel(deb.values()));
        
        loadCmb_princ();
    }
   

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        txtNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescrip = new javax.swing.JTextArea();
        cmbCuenta_princ = new javax.swing.JComboBox<>();
        cmbCuenta_aux = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbEstado = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtMoney = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtDescrip.setColumns(20);
        txtDescrip.setRows(5);
        jScrollPane1.setViewportView(txtDescrip);

        cmbCuenta_princ.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        cmbCuenta_princ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCuenta_princActionPerformed(evt);
            }
        });

        jLabel1.setText("Nombre de Comprobantes:");

        jLabel2.setText("Descripcion:");

        jLabel3.setText("Cuenta Principal Afectada:");

        jLabel4.setText("Cuenta Auxiliar Afectada:");

        cmbEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEstadoActionPerformed(evt);
            }
        });

        jLabel5.setText("Estado de afeccion de cuenta:");

        jButton1.setText("Agregar Comprobantes:");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel7.setText("Dinero:");

        txtMoney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMoneyActionPerformed(evt);
            }
        });
        txtMoney.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMoneyKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(23, 23, 23)
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(10, 10, 10)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(385, 385, 385)
                                            .addComponent(txtMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbCuenta_princ, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(cmbCuenta_aux, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(50, 50, 50)
                                    .addComponent(jLabel4)))
                            .addComponent(cmbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                                .addComponent(jButton1)
                                .addGap(142, 142, 142))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLabel7)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jLabel2)
                .addGap(15, 15, 15)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel7))
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCuenta_princ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbCuenta_aux, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMoney, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbCuenta_princActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCuenta_princActionPerformed
       loadCmb_aux();
    }//GEN-LAST:event_cmbCuenta_princActionPerformed

    public void Com_pasivo_Deb_activo_haber_capital_Deb(){
        
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        Implements_Comprobantes ic= new Implements_Comprobantes();
        if("".equals(txtNombre.getText()) ||txtDescrip.getText().equals("")||txtMoney.getText().equals("")){
           JOptionPane.showMessageDialog(this,
                        "ERROR, debe rellenar todos los campos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
           
                return;
      }
        
        Comprobantes act;
        fecha = new Date();
        act= new Comprobantes();
        
        act.setNombre(txtNombre.getText());
        act.setDescrpcion(txtDescrip.getText());
        act.setEstado( cmbEstado.getSelectedItem().toString());
        act.setCuenta_afecta(cmbCuenta_princ.getSelectedItem().toString());
        act.setCuenta_auxiliar(cmbCuenta_aux.getSelectedItem().toString());
        act.setFecha(fecha);
        Implements_Comprobantes oc= new Implements_Comprobantes();
        Implements_ER imp_er= new Implements_ER();
        Estado_Result ER= new Estado_Result();
        ER.setId(1);
         
        
        imp= new Implements_balance();
  
         try {
             List<Estado_Result> im= imp_er.showAll();
             ER= im.get(0);
             int i=0,j=0;
             List<Cuentas_balance> li= imp.showAll();
             List<Estado_Result> ER_List= imp_er.showAll();
             for(Cuentas_balance e: li){
                 if(e.getNombre_cuenta().equals(cmbCuenta_princ.getSelectedItem())){
                     i=e.getId_cuenta()-1;
                     
                     
                     
                 }
                  if(e.getNombre_cuenta().equals(cmbCuenta_aux.getSelectedItem())){
                     j=e.getId_cuenta()-1;
                      
                    
                 }
                 
             }
              
             
                 act.setCuentaER_afect("-");
                  act.setCantidad_hab(Double.parseDouble(txtMoney.getText()));
                  act.setCantidad_deb(Double.parseDouble(txtMoney.getText()));
                 if(cmbEstado.getSelectedItem()==deb.Haber){
                     
                 
                 if("Activo".equals(li.get(i).getTipo()) && !li.get(j).getTipo().equals(li.get(i).getTipo()) ){
                    
                     double afect= (-1)*Double.parseDouble(txtMoney.getText())+li.get(i).getDinero();
                     double afecte= (-1)*Double.parseDouble(txtMoney.getText())+li.get(j).getDinero();
                     
                     
                     if(li.get(i).getDinero()<Double.parseDouble(txtMoney.getText()) ||li.get(j).getDinero()<Double.parseDouble(txtMoney.getText()) ){
                         JOptionPane.showMessageDialog(this,
                        "ERROR, la cuenta afectada en el debe no tiene fondos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
           
                return;
                     }
                     
                     
                     li.get(i).setDinero(afect);
                     li.get(j).setDinero(afecte);
                    
                     imp.update(li.get(j));
                     imp.update(li.get(i));
                     ic.create(act);
                     reset();
                     
                     return;
                     
                 }
                 if(li.get(i).getTipo().equals("pasivo") && !li.get(i).getTipo().equals(li.get(j).getTipo())){
                     
                     System.out.println(li.get(i).getTipo());
                      double afect= Double.parseDouble(txtMoney.getText())+li.get(j).getDinero();
                     double afecte= Double.parseDouble(txtMoney.getText())+li.get(i).getDinero();
                     
                     
                     
                     li.get(i).setDinero(afecte);
                     li.get(j).setDinero(afect);
                     imp.update(li.get(j));
                     imp.update(li.get(i));
                     
                     ic.create(act);
                     reset();
                     return;
                     
                 }
                 if(li.get(j).getTipo().equals(li.get(i).getTipo())){
                     
                    double u=Double.parseDouble(txtMoney.getText());
                    
                    
                     double afect= li.get(i).getDinero()-u;
                     double afecte= li.get(j).getDinero()+u;
                     
                     
                     if(li.get(i).getDinero()<Double.parseDouble(txtMoney.getText()) ){
                         JOptionPane.showMessageDialog(this,
                        "ERROR, la cuenta afectada en el debe no tiene fondos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
           
                return;
                     }
                     
                     
                     li.get(i).setDinero(afect);
                     li.get(j).setDinero(afecte);
                    
                     imp.update(li.get(j));
                     imp.update(li.get(i));
                     reset();
                     ic.create(act);
                     
                     return;
                     
                 }
                
             }else
                 if(cmbEstado.getSelectedItem()==deb.Debe){
                if("Activo".equals(li.get(i).getTipo()) && !li.get(j).getTipo().equals(li.get(i).getTipo())){
                     double afect= Double.parseDouble(txtMoney.getText())+li.get(j).getDinero();
                     double afecte= Double.parseDouble(txtMoney.getText())+li.get(i).getDinero();
                    
                     
                    
                     
                     li.get(i).setDinero(afect);
                     li.get(j).setDinero(afecte);
                     imp.update(li.get(j));
                     imp.update(li.get(i));
                     reset();
                     ic.create(act);
                 }
                 if(li.get(i).getTipo().equals("pasivo") && !li.get(i).getTipo().equals(li.get(j).getTipo())){
                     
                     if(li.get(i).getDinero()<Double.parseDouble(txtMoney.getText()) ||li.get(j).getDinero()<Double.parseDouble(txtMoney.getText()) ){
                         JOptionPane.showMessageDialog(this,
                        "ERROR, la cuenta afectada en el debe no tiene fondos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
           
                return;
                     }
                    double afect= -Double.parseDouble(txtMoney.getText())+li.get(j).getDinero();
                     double afecte= -Double.parseDouble(txtMoney.getText())+li.get(i).getDinero();
                     
                    
                     
                     li.get(i).setDinero(afecte);
                     li.get(j).setDinero(afect);
                     imp.update(li.get(j));
                     imp.update(li.get(i));
                    ic.create(act);
                    reset();
                    return;
                     
                 }
                 if(li.get(j).getTipo().equals(li.get(i).getTipo())){
                     
                    double u=Double.parseDouble(txtMoney.getText());
                    
                    
                     double afect= li.get(i).getDinero()+u;
                     double afecte= li.get(j).getDinero()-u;
                     
                     
                     if(li.get(j).getDinero()<Double.parseDouble(txtMoney.getText()) ){
                         JOptionPane.showMessageDialog(this,
                        "ERROR, la cuenta afectada en el debe no tiene fondos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
           
                return;
                     }
                     
                    
                     li.get(i).setDinero(afect);
                     li.get(j).setDinero(afecte);
                    
                     imp.update(li.get(j));
                     imp.update(li.get(i));
                     ic.create(act);
                     reset();
                     return;
                     
                 }
                 
             }
             
             
            
             
         } catch (IOException ex) {
             Logger.getLogger(jAdd_comprobantes.class.getName()).log(Level.SEVERE, null, ex);
         }
        
   
    }//GEN-LAST:event_jButton1ActionPerformed

    public void reset(){
        txtDescrip.setText("");
        txtNombre.setText("");
        txtMoney.setText("");
        
    }
    private void cmbEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEstadoActionPerformed

    }//GEN-LAST:event_cmbEstadoActionPerformed

    private void txtMoneyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMoneyKeyTyped
        if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
        evt.consume();
    }
    if(evt.getKeyChar()=='.' && txtMoney.getText().contains(".")){
      evt.consume();  
    }
    }//GEN-LAST:event_txtMoneyKeyTyped

    private void txtMoneyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMoneyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMoneyActionPerformed

     public void loadCmb_aux(){
        
        int i= cmbCuenta_princ.getSelectedIndex()+1;
        int o= cmbCuenta_princ.getSelectedIndex();
        
        
        
        if(o<0){
            return;
        }
        
        cmbCuenta_aux.setVisible(true);
        
        cmbCuenta_aux.removeAllItems(); 
        
         try {
             
             List<Cuentas_balance> cuent=imp.showAll();
             if(0>=0){
                 if("pasivo".equals(cuent.get(i-1).getTipo())){
                         
                         
                     cuent.stream().filter(out -> (out.getId_cuenta()!=i && !"pasivo".equals(out.getTipo()))).forEachOrdered(out -> {
                         cmbCuenta_aux.addItem(out.getNombre_cuenta());
                     });
                         
                     }else{
                     cuent.stream().filter(out -> (out.getId_cuenta()!=i)).forEachOrdered(out -> {
                         cmbCuenta_aux.addItem(out.getNombre_cuenta());
                     });
                         
                     }
             }
                
                     
                     
                 
                 
                 
            
         } catch (IOException ex) {
             Logger.getLogger(jAdd_comprobantes.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public void loadCmb_princ(){
        List<Cuentas_balance> cuent= new ArrayList<>();
     
        imp= new Implements_balance();
        
       
           cmbCuenta_princ.removeAllItems(); 
        
        
        
         try {
             
             cuent=imp.showAll();
             
             for(Cuentas_balance out:cuent){
                cmbCuenta_princ.addItem(out.getNombre_cuenta());
            }
             
            
             
         } catch (IOException ex) {
             Logger.getLogger(jAdd_comprobantes.class.getName()).log(Level.SEVERE, null, ex);
         }
        
    }
    
    public static void main(String args[]) {
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jAdd_comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jAdd_comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jAdd_comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jAdd_comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jAdd_comprobantes dialog = new jAdd_comprobantes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cmbCuenta_aux;
    private javax.swing.JComboBox<String> cmbCuenta_princ;
    private javax.swing.JComboBox<String> cmbEstado;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtDescrip;
    private javax.swing.JTextField txtMoney;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
