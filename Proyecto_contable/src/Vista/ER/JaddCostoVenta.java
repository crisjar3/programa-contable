/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.ER;

import Cont.clases.BG.ER.COmpra;
import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_Comprobantes;
import DAO.Implements_ER;
import DAO.Implements_balance;
import DAO.Implements_compra;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class JaddCostoVenta extends javax.swing.JDialog {

    Date fecha= new Date();
    Implements_ER imp= new Implements_ER();
    Implements_balance impl= new Implements_balance();
    Implements_Comprobantes imple= new Implements_Comprobantes();
    public JaddCostoVenta(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbCuenta_aux = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbEstado = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescrip = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cmbCuenta_princ = new javax.swing.JComboBox<>();
        txtMoney = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        cmbCuenta_aux.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "INVENTARIO" }));

        jLabel1.setText("Nombre de Comprobantes:");

        jLabel2.setText("Descripcion:");

        jLabel3.setText("Cuenta Principal Afectada:");

        jLabel4.setText("Cuenta Auxiliar Afectada:");

        cmbEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "HABER" }));
        cmbEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEstadoActionPerformed(evt);
            }
        });

        jLabel5.setText("Estado de afeccion de cuenta:");

        txtDescrip.setColumns(20);
        txtDescrip.setRows(5);
        jScrollPane1.setViewportView(txtDescrip);

        jButton1.setText("Agregar Comprobantes:");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel7.setText("Dinero:");

        cmbCuenta_princ.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CAJA" }));
        cmbCuenta_princ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCuenta_princActionPerformed(evt);
            }
        });

        txtMoney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMoneyActionPerformed(evt);
            }
        });
        txtMoney.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMoneyKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(cmbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel5)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(cmbCuenta_princ, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(417, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(83, 83, 83)
                            .addComponent(jLabel1)
                            .addGap(10, 10, 10)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addComponent(jLabel2))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(50, 50, 50)
                            .addComponent(jLabel4)
                            .addGap(121, 121, 121)
                            .addComponent(jLabel7))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(192, 192, 192)
                            .addComponent(cmbCuenta_aux, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(93, 93, 93)
                            .addComponent(txtMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(390, 390, 390)
                            .addComponent(jButton1)))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(238, Short.MAX_VALUE)
                .addComponent(cmbCuenta_princ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 17, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(3, 3, 3)
                            .addComponent(jLabel1))
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(9, 9, 9)
                    .addComponent(jLabel2)
                    .addGap(15, 15, 15)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(30, 30, 30)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel3)
                        .addComponent(jLabel4)
                        .addComponent(jLabel7))
                    .addGap(6, 6, 6)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cmbCuenta_aux, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtMoney, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(24, 24, 24)
                    .addComponent(jButton1)
                    .addGap(0, 48, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEstadoActionPerformed

    }//GEN-LAST:event_cmbEstadoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

       
        try {
            List<Cuentas_balance> list= impl.showAll();
            Comprobantes act;
            fecha = new Date();
            act= new Comprobantes();

            act.setNombre(txtNombre.getText());
            act.setDescrpcion(txtDescrip.getText());
            act.setEstado( cmbEstado.getSelectedItem().toString());
            act.setCuenta_afecta(cmbCuenta_princ.getSelectedItem().toString());
            act.setCuenta_auxiliar(cmbCuenta_aux.getSelectedItem().toString());
            act.setFecha(fecha);
            act.setCantidad_hab(Double.parseDouble(txtMoney.getText()));
            act.setCantidad_deb(Double.parseDouble(txtMoney.getText()));
            
            act.setCuentaER_afect("COSTO DE VENTA");
            

            for(Cuentas_balance e: list){
                if(e.getNombre_cuenta().equals(cmbCuenta_princ.getSelectedItem().toString())){
                    double afe= e.getDinero()+Double.parseDouble(txtMoney.getText());

                    if(afe<0){
                        JOptionPane.showMessageDialog(this, "No hay fondos");
                        return;
                    }
                    e.setDinero(afe);

                    impl.update(e);

                }
                
                
            }
            for(Cuentas_balance e: list){
                
                    if(e.getNombre_cuenta().equals(cmbCuenta_aux.getSelectedItem().toString())){
                        double afe= e.getDinero()- Double.parseDouble(txtMoney.getText());
                        e.setDinero(afe);
                        impl.update(e);
                        
                        int n=imp.showAll().size()-1;
                        Estado_Result est= imp.showAll().get(n);
                        Implements_compra implComp= new Implements_compra();
                        double compra_tot=implComp.showAll().getDinero();
                        double compraAct= compra_tot+Double.parseDouble(txtMoney.getText());
                        double costo_venta= e.getDinero_inicial()+compraAct-afe;
                        COmpra compra= implComp.showAll();
                        compra.setDinero(compraAct);
                        implComp.update(compra);
                        est.setCosto_venta(costo_venta);
                        act.setCantidad_ER(costo_venta);
                        imple.create(act);
                        
                        
                        imp.update(est);
                        
                    }
            }
            JOptionPane.showMessageDialog(this, "listo");
            reset();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(JaddCostoVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
       

    }//GEN-LAST:event_jButton1ActionPerformed

    private void cmbCuenta_princActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCuenta_princActionPerformed

    }//GEN-LAST:event_cmbCuenta_princActionPerformed

    private void txtMoneyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMoneyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMoneyActionPerformed

    private void txtMoneyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMoneyKeyTyped
        if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
            evt.consume();
        }
        if(evt.getKeyChar()=='.' && txtMoney.getText().contains(".")){
            evt.consume();
        }
    }//GEN-LAST:event_txtMoneyKeyTyped

    public void reset(){
        txtDescrip.setText("");
        txtMoney.setText("");
        
        txtNombre.setText("");
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JaddCostoVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JaddCostoVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JaddCostoVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JaddCostoVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JaddCostoVenta dialog = new JaddCostoVenta(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbCuenta_aux;
    private javax.swing.JComboBox<String> cmbCuenta_princ;
    private javax.swing.JComboBox<String> cmbEstado;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtDescrip;
    private javax.swing.JTextField txtMoney;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
