
package Vista.ER;

import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_Comprobantes;
import DAO.Implements_ER;
import DAO.Implements_balance;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;


public class JDGAdd_Gastos extends javax.swing.JDialog {

    Date myDate = new Date();
    String fecha= new SimpleDateFormat("dd-MM-yyyy").format(myDate);
    
    enum gastos{
            Gastos_de_ventas,Gasto_de_administracion,Gastos_de_financiamiento 
    }
    
    Implements_ER impl= new Implements_ER();
    public JDGAdd_Gastos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        txtFecha.setText(fecha);
        txtFecha.setEditable(false);
        cmbGastos.setModel(new DefaultComboBoxModel(gastos.values()));
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        cmbGastos = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        txtCantidad = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Dinero que afecta el ER:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 110, -1));

        txtFecha.setText("jTextField1");
        getContentPane().add(txtFecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 30, 80, -1));

        cmbGastos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        getContentPane().add(cmbGastos, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 190, 20));

        jLabel2.setText("Agregando Gastos");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(49, 33, 173, -1));

        btnAdd.setText("Agregar Gasto");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        getContentPane().add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 200, -1, -1));

        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadKeyTyped(evt);
            }
        });
        getContentPane().add(txtCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 190, -1));

        jLabel3.setText("Seleccione el gasto:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 110, -1));

        setBounds(0, 0, 580, 339);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        try {                                       
            Implements_ER impl= new Implements_ER();
            Implements_balance ecl = new Implements_balance();
            for(Cuentas_balance e : ecl.showAll()){
                if(e.getNombre_cuenta().equals("BANCO")){
                    JOptionPane.showMessageDialog(this, e.getDinero());
                    double afe= e.getDinero()- Double.parseDouble(txtCantidad.getText());
                    JOptionPane.showMessageDialog(this, afe);
                    
                    if(afe<0){
                        JOptionPane.showMessageDialog(this, "No hay fondos");
                        return;
                    }
                    e.setDinero(afe);
                    
                    ecl.update(e);
                    
                }else if(e.getNombre_cuenta().equals("GANANCIAS")){
                        double din= e.getDinero()-Double.parseDouble(txtCantidad.getText());
                        e.setDinero(din);
                        ecl.update(e);
                    }
            }
            int last= impl.showAll().size()-1;
            Estado_Result ER= impl.showAll().get(last);
            Implements_Comprobantes imp = new Implements_Comprobantes();
            Comprobantes e= new Comprobantes();
            e.setNombre("Gastos");
            e.setCuentaER_afect(cmbGastos.getSelectedItem().toString());
            e.setCantidad_ER( Double.parseDouble( txtCantidad.getText()));
            e.setFecha(myDate);
            e.setDescrpcion("Pago de Gastos de tipo"+cmbGastos.getSelectedItem().toString());
            e.setEstado("-");
            e.setCantidad_deb(0.00);
            e.setCantidad_hab(Double.parseDouble( txtCantidad.getText()));
            e.setCuenta_afecta("BANCO");
            e.setCuenta_auxiliar("-");
            double gast= ER.getGasto_gen()+Double.parseDouble(txtCantidad.getText());
                ER.setGasto_gen(gast);
                impl.update(ER);
                
            
            reset();
            
                imp.create(e);
            
        } catch (IOException ex) {
            Logger.getLogger(JDGAdd_Gastos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void txtCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyTyped

 if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
            evt.consume();
        }
        if(evt.getKeyChar()=='.' && txtCantidad.getText().contains(".")){
            evt.consume();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadKeyTyped

    public void reset(){
        txtCantidad.setText("");
    }
    public static void main(String args[]) {
       
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDGAdd_Gastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDGAdd_Gastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDGAdd_Gastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDGAdd_Gastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDGAdd_Gastos dialog = new JDGAdd_Gastos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JComboBox<String> cmbGastos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtFecha;
    // End of variables declaration//GEN-END:variables
}
