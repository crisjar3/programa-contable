
package Vista.ER;

import Cont.clases.BG.ER.Comprobantes;
import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import DAO.Implements_Comprobantes;
import DAO.Implements_ER;
import DAO.Implements_balance;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class jAddVentas extends javax.swing.JDialog {

    Date fecha= new Date();
    Implements_ER imp= new Implements_ER();
    Implements_balance impl= new Implements_balance();
    Implements_Comprobantes imple= new Implements_Comprobantes();
    public jAddVentas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbCuenta_aux = new javax.swing.JComboBox<>();
        txtMoney_secondary = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbEstado = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescrip = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cmbCuenta_princ = new javax.swing.JComboBox<>();
        txtMoney = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cmbCuenta_aux.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CAJA" }));
        getContentPane().add(cmbCuenta_aux, new org.netbeans.lib.awtextra.AbsoluteConstraints(202, 246, 110, -1));

        txtMoney_secondary.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMoney_secondaryKeyTyped(evt);
            }
        });
        getContentPane().add(txtMoney_secondary, new org.netbeans.lib.awtextra.AbsoluteConstraints(202, 299, 120, -1));

        jLabel1.setText("Nombre de Comprobantes:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(93, 45, -1, -1));

        jLabel2.setText("Descripcion:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 71, -1, -1));

        jLabel3.setText("Cuenta Principal Afectada:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 226, -1, -1));

        jLabel4.setText("Cuenta Auxiliar Afectada:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(188, 226, -1, -1));

        cmbEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "HABER" }));
        cmbEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEstadoActionPerformed(evt);
            }
        });
        getContentPane().add(cmbEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 325, 125, -1));

        jLabel5.setText("Estado de afeccion de cuenta:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, -1, -1));
        getContentPane().add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(232, 42, 216, -1));

        jLabel6.setText("Dinero de cuenta secundaria:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(202, 267, -1, -1));

        txtDescrip.setColumns(20);
        txtDescrip.setRows(5);
        jScrollPane1.setViewportView(txtDescrip);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 430, -1));

        jButton1.setText("Agregar Comprobantes:");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 290, -1, -1));

        jLabel7.setText("Dinero:");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(433, 226, -1, -1));

        cmbCuenta_princ.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "INVENTARIO" }));
        cmbCuenta_princ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCuenta_princActionPerformed(evt);
            }
        });
        getContentPane().add(cmbCuenta_princ, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 246, 125, -1));

        txtMoney.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMoneyActionPerformed(evt);
            }
        });
        txtMoney.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMoneyKeyTyped(evt);
            }
        });
        getContentPane().add(txtMoney, new org.netbeans.lib.awtextra.AbsoluteConstraints(405, 246, 95, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtMoney_secondaryKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMoney_secondaryKeyTyped
        if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
            evt.consume();
        }
        if(evt.getKeyChar()=='.' && txtMoney.getText().contains(".")){
            evt.consume();
        }
    }//GEN-LAST:event_txtMoney_secondaryKeyTyped

    private void cmbEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEstadoActionPerformed

    }//GEN-LAST:event_cmbEstadoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        try {
            List<Cuentas_balance> list= impl.showAll();
            Comprobantes act;
        fecha = new Date();
        act= new Comprobantes();
        
        act.setNombre(txtNombre.getText());
        act.setDescrpcion(txtDescrip.getText());
        act.setEstado( cmbEstado.getSelectedItem().toString());
        act.setCuenta_afecta(cmbCuenta_princ.getSelectedItem().toString());
        act.setCuenta_auxiliar(cmbCuenta_aux.getSelectedItem().toString());
        act.setFecha(fecha);
        act.setCantidad_hab(Double.parseDouble(txtMoney.getText()));
        act.setCantidad_deb(Double.parseDouble(txtMoney_secondary.getText()));
        act.setCantidad_ER(Double.parseDouble(txtMoney_secondary.getText()));
        act.setCuentaER_afect("VENTA");
        imple.create(act);
        
            for(Cuentas_balance e: list){
                if(e.getNombre_cuenta().equals("GANANCIAS")){
                        double din= e.getDinero()+Double.parseDouble(txtMoney_secondary.getText())-Double.parseDouble(txtMoney.getText());
                        e.setDinero(din);
                        impl.update(e);
                    }
                if(e.getNombre_cuenta().equals(cmbCuenta_princ.getSelectedItem().toString())){
                    double afe= e.getDinero()- Double.parseDouble(txtMoney.getText());
                    
                    if(afe<0){
                        JOptionPane.showMessageDialog(this, "No hay fondos");
                        return;
                    }
                    e.setDinero(afe);
                    
                    impl.update(e);
                    
                }else
                    if(e.getNombre_cuenta().equals(cmbCuenta_aux.getSelectedItem().toString())){
                        
                    double afe= e.getDinero()+ Double.parseDouble(txtMoney_secondary.getText());
                    e.setDinero(afe);
                    impl.update(e);
                    
                    int n=imp.showAll().size()-1;
                    Estado_Result est= imp.showAll().get(n);
                   
                    Double vet= est.getVenta()+ Double.parseDouble(txtMoney_secondary.getText());
                    est.setVenta(vet);
                    
                    imp.update(est);
                    
                }
                
            }
            for(Cuentas_balance e: list){
                if(e.getNombre_cuenta().equals("GANANCIAS")){
                        double din= e.getDinero()+Double.parseDouble(txtMoney_secondary.getText())-Double.parseDouble(txtMoney.getText());
                        e.setDinero(din);
                        impl.update(e);
                    }
                
                    if(e.getNombre_cuenta().equals(cmbCuenta_aux.getSelectedItem().toString())){
                        
                    double afe= e.getDinero()+ Double.parseDouble(txtMoney_secondary.getText());
                    e.setDinero(afe);
                    impl.update(e);
                    
                    int n=imp.showAll().size()-1;
                    Estado_Result est= imp.showAll().get(n);
                   
                    Double vet= est.getVenta()+ Double.parseDouble(txtMoney_secondary.getText());
                    est.setVenta(vet);
                    
                    imp.update(est);
                    
                }
                
            }
            JOptionPane.showMessageDialog(this, "listo");
            reset();
        } catch (IOException ex) {
            Logger.getLogger(jAddVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cmbCuenta_princActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCuenta_princActionPerformed
      
    }//GEN-LAST:event_cmbCuenta_princActionPerformed

    private void txtMoneyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMoneyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMoneyActionPerformed

    private void txtMoneyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMoneyKeyTyped
        if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
            evt.consume();
        }
        if(evt.getKeyChar()=='.' && txtMoney.getText().contains(".")){
            evt.consume();
        }
    }//GEN-LAST:event_txtMoneyKeyTyped

    public void reset(){
        txtDescrip.setText("");
        txtMoney.setText("");
        txtMoney_secondary.setText("");
        txtNombre.setText("");
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jAddVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jAddVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jAddVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jAddVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jAddVentas dialog = new jAddVentas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbCuenta_aux;
    private javax.swing.JComboBox<String> cmbCuenta_princ;
    private javax.swing.JComboBox<String> cmbEstado;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtDescrip;
    private javax.swing.JTextField txtMoney;
    private javax.swing.JTextField txtMoney_secondary;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}