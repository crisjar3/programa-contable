
package Vista.Razones;

import Cont.clases.BG.ER.Cuentas_balance;
import Cont.clases.BG.ER.Estado_Result;
import Cont.clases.BG.ER.Gastos;
import Cont.clases.razones.RazonesFinancieras;
import DAO.Implements_ER;
import DAO.Implements_balance;
import DAO.Implements_gastos;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;


public class Razones extends javax.swing.JDialog {

    enum types{
        Liquidez,Actividad,Endeudamiento,Rentabilidad
    }
    enum Liquidez{
        Capital_de_Trabajo,Indice_de_solvencia,Prueba_Acida
    }
    enum Actividad{
        Rotacion_Inventario,Periodo_Promedio_de_Cobro,Rotacion_de_Activo_Fijo,
        Rotacion_activos_Totales
    }
    enum Endeudamiento{
        Deuda_Total,Razon_Pasivo_Capital,Rotacion_de_intereses_de_utilidades
    }
    enum Rentabilidad{
        Margen_utilidad_bruta,Margen_utilidad_Operativo,Margen_utilidad_Neta,Rendimiento_de_Netos
    }
    public Razones(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cmbTipos.setModel(new DefaultComboBoxModel(types.values()));
        cmbSecond.setVisible(false);
        lblProm.setVisible(false);
        txtPromedioIndustria.setVisible(false);
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbTipos = new javax.swing.JComboBox<>();
        cmbSecond = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtPromedioIndustria = new javax.swing.JTextField();
        lblProm = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Yu Gothic UI Semilight", 3, 18)); // NOI18N
        jLabel1.setText("Revisar Razones Financieras");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 260, 40));

        cmbTipos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTiposActionPerformed(evt);
            }
        });
        getContentPane().add(cmbTipos, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 190, -1));

        cmbSecond.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbSecond.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSecondItemStateChanged(evt);
            }
        });
        cmbSecond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSecondActionPerformed(evt);
            }
        });
        getContentPane().add(cmbSecond, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 190, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText(":Tipo");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 90, 60, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Revisar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 110, -1));

        txtPromedioIndustria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPromedioIndustriaKeyTyped(evt);
            }
        });
        getContentPane().add(txtPromedioIndustria, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 60, 80, -1));

        lblProm.setText("Promedio de la Industria:");
        getContentPane().add(lblProm, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 60, -1, -1));

        setSize(new java.awt.Dimension(563, 339));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cmbTiposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTiposActionPerformed
        //Liquidez,Actividad,Endeudamiento,Rentabilidad
        cmbSecond.setVisible(true);
        switch (cmbTipos.getSelectedItem().toString()) {
            case "Liquidez":
                cmbSecond.setModel(new DefaultComboBoxModel(Liquidez.values()));
                break;
            case "Actividad":
                cmbSecond.setModel(new DefaultComboBoxModel(Actividad.values()));
                break;
            case "Endeudamiento":
                cmbSecond.setModel(new DefaultComboBoxModel(Endeudamiento.values()));
                break;
            case "Rentabilidad":
                cmbSecond.setModel(new DefaultComboBoxModel(Rentabilidad.values()));
                break;
            default:
                break;
        }
        if(cmbSecond.getSelectedItem().equals(Actividad.Rotacion_Inventario)){
            lblProm.setVisible(true);
            txtPromedioIndustria.setVisible(true);
        }else{
            lblProm.setVisible(false);
            txtPromedioIndustria.setVisible(false);
        }
    }//GEN-LAST:event_cmbTiposActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         try {
        if(cmbSecond.getSelectedItem().equals(Liquidez.Capital_de_Trabajo)){
       
            JOptionPane.showMessageDialog(this, RazonesFinancieras.capitaldetrabajo());    
        }if(cmbSecond.getSelectedItem().equals(Liquidez.Indice_de_solvencia)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.indice_de_solvencia(Double.parseDouble(txtPromedioIndustria.getText()))); 
        }if(cmbSecond.getSelectedItem().equals(Liquidez.Prueba_Acida)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.pruebaAcida(DinCuent("INVENTARIO"), Double.parseDouble(txtPromedioIndustria.getText()))); 
        }if(cmbSecond.getSelectedItem().equals(Actividad.Rotacion_Inventario)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RotacionDeInventario(DinCuent("INVENTARIO"), Double.parseDouble(txtPromedioIndustria.getText())));
        }if(cmbSecond.getSelectedItem().equals(Actividad.Periodo_Promedio_de_Cobro)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.PeriodoPromedioCobro(DinVentAct(), DinCuent("CUENTAS POR COBRAR")));
            //JOptionPane.showMessageDialog(this, DinVentAct());
        }if(cmbSecond.getSelectedItem().equals(Actividad.Rotacion_de_Activo_Fijo)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RotacionDeActFijo(DinVentAct(), Double.parseDouble(txtPromedioIndustria.getText())));
        } if(cmbSecond.getSelectedItem().equals(Actividad.Rotacion_activos_Totales)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RotacionDeActivosTotales(DinVentAct()));
        }if(cmbSecond.getSelectedItem().equals(Endeudamiento.Deuda_Total)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RazonDeDeudaTotal());
        }if(cmbSecond.getSelectedItem().equals(Endeudamiento.Razon_Pasivo_Capital)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RazonPasivoaCapital());
        }if(cmbSecond.getSelectedItem().equals(Endeudamiento.Rotacion_de_intereses_de_utilidades)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RazonDeRotacionDeInteresaUtilidades(UtlidAntesImp(), GastInteres()));
        }if(cmbSecond.getSelectedItem().equals(Rentabilidad.Margen_utilidad_bruta)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.MargenUtilidadBruta(CostVent(), DinVentAct()));
        }if(cmbSecond.getSelectedItem().equals(Rentabilidad.Margen_utilidad_Operativo)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.MargeUtilidadOperativa(CostVent(), DinVentAct()));
        }if(cmbSecond.getSelectedItem().equals(Rentabilidad.Margen_utilidad_Neta)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.MargenUtilidadNeta(UtlidespImp(), DinVentAct()));
        }if(cmbSecond.getSelectedItem().equals(Rentabilidad.Rendimiento_de_Netos)){
            JOptionPane.showMessageDialog(this, RazonesFinancieras.RendimientosActivos(UtlidespImp()));
        }
    }catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cmbSecondItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSecondItemStateChanged
      
    }//GEN-LAST:event_cmbSecondItemStateChanged

    private void txtPromedioIndustriaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPromedioIndustriaKeyTyped
         if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
            evt.consume();
        }
        if(evt.getKeyChar()=='.' && txtPromedioIndustria.getText().contains(".")){
            evt.consume();
        }
    }//GEN-LAST:event_txtPromedioIndustriaKeyTyped

    private void cmbSecondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSecondActionPerformed
         if(cmbSecond.getSelectedItem().equals(Liquidez.Indice_de_solvencia)|| cmbSecond.getSelectedItem().equals(Liquidez.Prueba_Acida)||cmbSecond.getSelectedItem().equals(Actividad.Rotacion_Inventario)||cmbSecond.getSelectedItem().equals(Actividad.Rotacion_de_Activo_Fijo)){
            lblProm.setVisible(true);
            txtPromedioIndustria.setVisible(true);
        }else{
             lblProm.setVisible(false);
            txtPromedioIndustria.setVisible(false);
         }
    }//GEN-LAST:event_cmbSecondActionPerformed

    public double DinCuent(String nomb){
        double din=0.00;
        try {
            
            Implements_balance implg= new Implements_balance();
            List<Cuentas_balance> list=implg.showAll();
            for(Cuentas_balance e : list){
                if(e.getNombre_cuenta().equals(nomb)){
                    din= e.getDinero();
                    return din;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
   
    
    public double DinVentAct(){
        double din=0.00;
        try {
            
            Implements_ER impl= new Implements_ER();
            List<Estado_Result> list= impl.showAll();
            
            
            din= list.get(list.size()-1).getVenta();
            return din;
            
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
    public double GastInteres(){
        double din=0.00;
        Date fecha= new Date();
        try {
            
            Implements_gastos impl= new Implements_gastos();
            List<Gastos> list= impl.showAll();
            
            
            for(Gastos e : list){
                if(e.getNombre().equals("Gastos por intereses")){
                    if(e.getFecha_Final().before(fecha)){
                        din+=e.getMensualidad();
                    }
                    
                }
            }
            return din;
            
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
    public double UtlidAntesImp(){
        double din=0.00;
        try {
            
            Implements_ER impl= new Implements_ER();
            Estado_Result ER= impl.showAll().get(impl.showAll().size()-1);
            din=ER.getVenta()-ER.getCosto_venta()-ER.getGasto_gen();
            return din;
            
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
    public double UtlidespImp(){
        double din=0.00;
        try {
            
            Implements_ER impl= new Implements_ER();
            Estado_Result ER= impl.showAll().get(impl.showAll().size()-1);
            din=ER.getVenta()-ER.getCosto_venta()-ER.getGasto_gen()-ER.getDividendos_comunes()-ER.getDividendos_preferentes()-UtlidAntesImp()*0.15;
            return din;
            
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
    public double UtlidOp(){
        double din=0.00;
        try {
            
            Implements_ER impl= new Implements_ER();
            Estado_Result ER= impl.showAll().get(impl.showAll().size()-1);
            din=ER.getVenta()-ER.getCosto_venta()-ER.getGasto_gen();
            return din;
            
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
    public double CostVent(){
        double din=0.00;
        try {
            
            Implements_ER impl= new Implements_ER();
            Estado_Result ER= impl.showAll().get(impl.showAll().size()-1);
            din=ER.getCosto_venta();
            return din;
            
        } catch (IOException ex) {
            Logger.getLogger(Razones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return din;
    }
    public int Confirm(){
        if(txtPromedioIndustria.getText().equals("")){
            return 0;
        }
        return 1;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Razones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Razones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Razones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Razones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Razones dialog = new Razones(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbSecond;
    private javax.swing.JComboBox<String> cmbTipos;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblProm;
    private javax.swing.JTextField txtPromedioIndustria;
    // End of variables declaration//GEN-END:variables
}
