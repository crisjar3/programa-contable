
package Vista.BG;

import Cont.clases.BG.ER.Cuentas_balance;
import DAO.Implements_balance;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import model.Tabmod;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

public class Admin_cuentas_balance extends javax.swing.JDialog {

    Tabmod model_BG;
    Implements_balance imp = new Implements_balance();

    public Admin_cuentas_balance(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        load();

    }

    public void load() {
        model_BG = new Tabmod();
        try {
            model_BG.loadFromFile();
        } catch (IOException ex) {
            Logger.getLogger(Admin_cuentas_balance.class.getName()).log(Level.SEVERE, null, ex);
        }
        tblEstudiantes.setModel(model_BG);
        pnlOK.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBoton = new javax.swing.JPanel();
        btnBG = new javax.swing.JButton();
        panel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEstudiantes = new javax.swing.JTable();
        pnlOK = new javax.swing.JPanel();
        btnOK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        pnlBoton.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        btnBG.setText("Reporte BG");
        btnBG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBGActionPerformed(evt);
            }
        });
        pnlBoton.add(btnBG);

        getContentPane().add(pnlBoton, java.awt.BorderLayout.NORTH);

        panel.setLayout(new java.awt.BorderLayout());

        tblEstudiantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Apellido", "Nota"
            }
        ));
        jScrollPane1.setViewportView(tblEstudiantes);

        panel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });
        pnlOK.add(btnOK);

        getContentPane().add(pnlOK, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(554, 337));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /*
   
    */
    private void btnBGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBGActionPerformed
        try {
            List<Cuentas_balance> activosCircDisp = new ArrayList<>();
            List<Cuentas_balance> activosCircReal = new ArrayList<>();
            List<Cuentas_balance> activosNoCircFijo = new ArrayList<>();
            List<Cuentas_balance> activosNoCircIntan = new ArrayList<>();
            List<Cuentas_balance> activosNoCircOtros = new ArrayList<>();
            List<Cuentas_balance> pasivosCirc = new ArrayList<>();
            List<Cuentas_balance> pasivosNoCircFijo = new ArrayList<>();
            List<Cuentas_balance> pasivosNoCircDif = new ArrayList<>();
            List<Cuentas_balance> capitalContr = new ArrayList<>();
            List<Cuentas_balance> capitalGan = new ArrayList<>();

            List<Cuentas_balance> lis = imp.showAll();

            List<Cuentas_balance> cuentas = new ArrayList<>();
            Cuentas_balance cuenta;

            for (int i = 0; i < lis.size(); i++) {
                cuenta = lis.get(i);
                if ("Activo".equals(cuenta.getTipo())) {
                    if ("circulante".equals(cuenta.getTiempo())) {
                        if ("disponible".equals(cuenta.getFinali())) {
                            activosCircDisp.add(cuenta);
                        } else {
                            activosCircReal.add(cuenta);
                        }
                    } else {
                        if ("fijo".equals(cuenta.getFinali())) {
                            activosNoCircFijo.add(cuenta);
                        } else {
                            if ("intangible".equals(cuenta.getFinali())) {
                                activosNoCircIntan.add(cuenta);
                            } else {
                                activosNoCircOtros.add(cuenta);
                            }
                        }
                    }
                } else {
                    if ("pasivo".equals(cuenta.getTipo())) {
                        if ("circulante".equals(cuenta.getTiempo())) {
                            pasivosCirc.add(cuenta);
                        } else {
                            if ("fijo".equals(cuenta.getFinali())) {
                                pasivosNoCircFijo.add(cuenta);
                            } else {
                                pasivosNoCircDif.add(cuenta);
                            }
                        }
                    } else {
                        if ("capital".equals(cuenta.getTipo())) {
                            if ("contribuido".equals(cuenta.getFinali())) {
                                capitalContr.add(cuenta);
                            } else {
                                capitalGan.add(cuenta);
                            }
                        }
                    }
                }
            }

            cuentas.addAll(activosCircDisp);
            cuentas.addAll(activosCircReal);
            cuentas.addAll(activosNoCircFijo);
            cuentas.addAll(activosNoCircIntan);
            cuentas.addAll(activosNoCircOtros);
            cuentas.addAll(pasivosCirc);
            cuentas.addAll(pasivosNoCircFijo);
            cuentas.addAll(pasivosNoCircDif);
            cuentas.addAll(capitalContr);
            cuentas.addAll(capitalGan);

            JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(cuentas);

            Map<String, Object> params = new HashMap<>();

            
            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile("src/resources/BalanceGeneral.jasper");
            
            InputStream imgInputStream = this.getClass().getResourceAsStream("/resources/logo.png");
            
            params.put("logo", imgInputStream);
         
            params.put("num", capitalContr.size() + capitalGan.size());

            JasperPrint jrPrint;
            jrPrint = JasperFillManager.
                    fillReport(jasperReport, params, datasource);

            btnBG.setEnabled(false);
            panel.remove(jScrollPane1);
            panel.add(new JRViewer(jrPrint));
            
            this.setSize(this.getToolkit().getScreenSize());  
            pnlOK.setVisible(true);
        } catch (IOException | JRException ex) {
            Logger.getLogger(Admin_cuentas_balance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnBGActionPerformed

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        panel.removeAll();
        pnlOK.setVisible(false);
        panel.add(jScrollPane1);
        btnBG.setEnabled(true);
    }//GEN-LAST:event_btnOKActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin_cuentas_balance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin_cuentas_balance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin_cuentas_balance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin_cuentas_balance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Admin_cuentas_balance dialog = new Admin_cuentas_balance(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBG;
    private javax.swing.JButton btnOK;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel;
    private javax.swing.JPanel pnlBoton;
    private javax.swing.JPanel pnlOK;
    private javax.swing.JTable tblEstudiantes;
    // End of variables declaration//GEN-END:variables

}
