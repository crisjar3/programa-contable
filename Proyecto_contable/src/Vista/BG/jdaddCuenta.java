/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.BG;

import Cont.clases.BG.ER.Cuentas_balance;
import DAO.Implements_balance;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Cristhian Joel Gomez
 */
public class jdaddCuenta extends javax.swing.JDialog {

    Implements_balance imp ;
    enum tipo{
        Activo,pasivo,capital
    }
    enum clasific_circ{
        circulante,no_circulante
    }
    enum act_cir{
        disponible,realizable
    }
    enum act_nocirc{
        fijo,intangible,otros
    }
    enum pas_noCirc{
        fijo,diferido
    }
    enum capita{
        contribuido,ganado
    }
    
    public jdaddCuenta(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        cmbfinal.setVisible(false);
        cmbTime.setVisible(false);
        cmbTipo.setModel(new DefaultComboBoxModel(tipo.values()));
        
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbTipo = new javax.swing.JComboBox<>();
        cmbfinal = new javax.swing.JComboBox<>();
        cmbTime = new javax.swing.JComboBox<>();
        txtNombre = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtDin = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoActionPerformed(evt);
            }
        });

        cmbfinal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cmbTime.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTimeActionPerformed(evt);
            }
        });

        jButton1.setText("Agregar Cuenta");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Configuarcion de cuenta");

        jLabel3.setText("Nombre de la cuenta:");

        jLabel2.setText("Cantidad Inicial:");

        txtDin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDinActionPerformed(evt);
            }
        });
        txtDin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDinKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(77, 77, 77))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbfinal, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbTime, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(159, 159, 159)
                        .addComponent(txtDin)))
                .addGap(27, 109, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(60, 60, 60)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(cmbfinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jLabel2)))
                .addGap(18, 18, 18)
                .addComponent(txtDin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(35, 35, 35))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      if("".equals(txtNombre.getText())){
           JOptionPane.showMessageDialog(this,
                        "ERROR, debe rellenar todos los campos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
                return;
      }
      if(cmbfinal.isVisible()==false && cmbTime.isVisible()==true && cmbTipo.isVisible()==true){
          Cuentas_balance e= new Cuentas_balance();
          imp=  new Implements_balance();
          
          e.setNombre_cuenta(txtNombre.getText());
          e.setTipo(cmbTipo.getSelectedItem().toString());
          e.setTiempo(cmbTime.getSelectedItem().toString());
          e.setFinali("-");
          e.setDinero(Double.parseDouble(txtDin.getText()));
          e.setDinero_inicial(Double.parseDouble(txtDin.getText()));
          try {
              imp.create(e);
              reset();
          } catch (IOException ex) {
              Logger.getLogger(jdaddCuenta.class.getName()).log(Level.SEVERE, null, ex);
          }
          return;
          
          
      }
      if(cmbfinal.isVisible()==true && cmbTime.isVisible()==false && cmbTipo.isVisible()==true){
          Cuentas_balance e= new Cuentas_balance();
          imp=  new Implements_balance();
          e.setNombre_cuenta(txtNombre.getText());
          e.setTipo(cmbTipo.getSelectedItem().toString());
          e.setTiempo("-");
          e.setFinali(cmbfinal.getSelectedItem().toString());
          e.setDinero(Double.parseDouble(txtDin.getText()));
          e.setDinero_inicial(Double.parseDouble(txtDin.getText()));
          try {
              imp.create(e);
              reset();
          } catch (IOException ex) {
              Logger.getLogger(jdaddCuenta.class.getName()).log(Level.SEVERE, null, ex);
          } 
          return;
          
      }
      if(cmbfinal.isVisible()==true && cmbTime.isVisible()==true && cmbTipo.isVisible()==true){
          Cuentas_balance e= new Cuentas_balance();
          imp=  new Implements_balance();
          e.setNombre_cuenta(txtNombre.getText());
          e.setTipo(cmbTipo.getSelectedItem().toString());
          e.setTiempo(cmbTime.getSelectedItem().toString());
          e.setFinali(cmbfinal.getSelectedItem().toString());
          e.setDinero(Double.parseDouble(txtDin.getText()));
          e.setDinero_inicial(Double.parseDouble(txtDin.getText()));
          try {
              imp.create(e);
              reset();
          } catch (IOException ex) {
              Logger.getLogger(jdaddCuenta.class.getName()).log(Level.SEVERE, null, ex);
          } 
          return;
          
      }else
          if(cmbTipo.isVisible()==true && cmbfinal.isVisible()==true && cmbTipo.isVisible()==true){
               Cuentas_balance e= new Cuentas_balance();
          imp=  new Implements_balance();
          e.setNombre_cuenta(txtNombre.getText());
          e.setTipo(cmbTipo.getSelectedItem().toString());
          e.setTiempo("-");
          e.setFinali(cmbfinal.getSelectedItem().toString());
          e.setDinero(Double.parseDouble(txtDin.getText()));
          e.setDinero_inicial(Double.parseDouble(txtDin.getText()));
          try {
              imp.create(e);
              reset();
          } catch (IOException ex) {
              Logger.getLogger(jdaddCuenta.class.getName()).log(Level.SEVERE, null, ex);
          } 
          return;
          }else
          if(cmbTipo.isVisible()==true && cmbTime.isVisible()==true){
               JOptionPane.showMessageDialog(this,
                        "ERROR, debe rellenar todos los campos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
                return;
          }else
              if(cmbTipo.isVisible()==true){
                   JOptionPane.showMessageDialog(this,
                        "ERROR, debe rellenar todos los campos",
                        "Ventana de Error", JOptionPane.ERROR_MESSAGE);
                return;
              }
    }//GEN-LAST:event_jButton1ActionPerformed

    public void reset(){
        cmbTime.setVisible(false);
              cmbfinal.setVisible(false);
              txtDin.setText("");
              txtNombre.setText("");
    }
    private void cmbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoActionPerformed
       if(cmbTipo.getSelectedItem()==tipo.Activo || cmbTipo.getSelectedItem()==tipo.pasivo){
       
           cmbTime.setVisible(true);
            cmbfinal.setVisible(false);
           cmbTime.setModel(new DefaultComboBoxModel(clasific_circ.values()));
       }else
       if(cmbTipo.getSelectedItem()==tipo.capital){
           cmbfinal.setVisible(true);
           cmbTime.setVisible(false);
           cmbfinal.setModel(new DefaultComboBoxModel(capita.values()));
           
       }
    }//GEN-LAST:event_cmbTipoActionPerformed

    private void cmbTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTimeActionPerformed
        if(cmbTime.getSelectedItem()==clasific_circ.circulante && cmbTipo.getSelectedItem()==tipo.Activo){
            cmbfinal.setVisible(true);
            cmbfinal.setModel(new DefaultComboBoxModel(act_cir.values()));
        }else
        if(cmbTime.getSelectedItem()==clasific_circ.no_circulante && cmbTipo.getSelectedItem()==tipo.Activo){
            cmbfinal.setVisible(true);
            cmbfinal.setModel(new DefaultComboBoxModel(act_nocirc.values()));
        }else
            if(cmbTime.getSelectedItem()==clasific_circ.no_circulante && cmbTipo.getSelectedItem()==tipo.pasivo){
            cmbfinal.setVisible(true);
            cmbfinal.setModel(new DefaultComboBoxModel(pas_noCirc.values()));
        }else
           if(cmbTime.getSelectedItem()==clasific_circ.circulante && cmbTipo.getSelectedItem()==tipo.pasivo){
                    cmbfinal.setVisible(false);
                }
    }//GEN-LAST:event_cmbTimeActionPerformed

    private void txtDinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDinActionPerformed

    private void txtDinKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDinKeyTyped
    if(!Character.isDigit(evt.getKeyChar()) &&  evt.getKeyChar()!='.'){
        evt.consume();
    }
    if(evt.getKeyChar()=='.' && txtDin.getText().contains(".")){
      evt.consume();  
    }
        
    }//GEN-LAST:event_txtDinKeyTyped

    public static void solonumber(final JTextField a){
        a.addKeyListener(new KeyAdapter() {
        public void KeyTyped(KeyEvent evt){
            int k=(int)evt.getKeyChar();
            if(k>=46 && k<=57){
                if(k==46){
                    String dato= a.getText();
                    int tam= dato.length();
                    for(int i=0 ;i<tam; i++){
                        if(dato.contains(".")){
                           evt.setKeyChar((char)KeyEvent.VK_CLEAR);
                        }
                    }
                }if(k==47){
                    evt.setKeyChar((char)KeyEvent.VK_CLEAR);
                }
                
            }else{
                evt.setKeyChar((char)KeyEvent.VK_CLEAR);
                evt.consume();
            }
        }
        });
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jdaddCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jdaddCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jdaddCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jdaddCuenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jdaddCuenta dialog = new jdaddCuenta(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbTime;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JComboBox<String> cmbfinal;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField txtDin;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
